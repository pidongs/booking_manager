<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('checkin');
            $table->string('checkout');
            $table->unsignedInteger('room')->nullable();
            $table->unsignedInteger('nights');
            $table->unsignedInteger('guests');
            $table->string('customer_name');
            $table->text('customer_address')->nullable();
            $table->string('customer_country')->default('PH');
            $table->string('customer_email');
            $table->string('customer_phone')->nullable();
            $table->text('comment')->nullable();
            $table->decimal('price', 10, 2)->default('0.00');
            $table->decimal('paid', 10, 2)->default('0.00');
            $table->decimal('total', 10, 2)->default('0.00');
            $table->string('staff')->nullable();
            $table->unsignedInteger('invoice_number')->default(0);
            $table->string('status')->default('New');
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
