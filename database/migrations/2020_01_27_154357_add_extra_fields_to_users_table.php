<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->boolean('owner_rights')->default(0);
            $table->boolean('booking_rights')->default(0);
            $table->boolean('datepicker_rights')->default(0);
            $table->boolean('admin_rights')->default(0);
            $table->boolean('calendar_rights')->default(0);
            $table->boolean('cc_rights')->default(0);
            $table->boolean('channelmanager_rights')->default(0);
            $table->boolean('settings_rights')->default(0);
            $table->boolean('delete_rights')->default(0);
            $table->boolean('print_rights')->default(0);
            $table->string('created_ip')->nullable();
            $table->string('last_ip')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->unsignedInteger('property_info_id')->nullable();
            $table->unsignedInteger('invoice_info_id')->nullable();
            $table->unsignedInteger('payment_info_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
