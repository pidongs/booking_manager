<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('requireprepay')->nullable();
            $table->unsignedInteger('prepaymore7d')->default(0)->nullable();
            $table->unsignedInteger('prepayless7d')->default(0)->nullable();
            $table->text('prepay')->nullable();
            $table->text('prepay_success')->nullable();
            $table->text('prepay_fail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_infos');
    }
}
