<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('requirecc')->default(0)->nullable();
            $table->string('type')->nullable();
            $table->string('email')->unique()->nullable();
            $table->text('logourl')->nullable();
            $table->text('coordinates')->nullable();
            $table->string('country')->default('PH')->nullable();
            $table->string('city')->nullable();
            $table->text('address')->nullable();
            $table->string('postalcode')->nullable();
            $table->text('tandc')->nullable();
            $table->unsignedInteger('mindaysstay')->default(0)->nullable();
            $table->text('otherinfo')->nullable();
            $table->time('checkin')->nullable();
            $table->time('checkout')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_infos');
    }
}
