<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('logourl')->nullable();
            $table->unsignedInteger('vat')->nullable();
            $table->string('invoiceprefix')->nullable();
            $table->string('currency')->default('PHP')->nullable();
            $table->text('companydetails')->nullable();
            $table->text('description')->nullable();
            $table->boolean('sendinvoice')->default(0)->nullable();
            $table->text('invoicetext')->nullable();
            $table->boolean('sendapproval')->default(0)->nullable();
            $table->text('approvaltext')->nullable();
            $table->text('invoicefooter')->nullable();
            $table->text('signaturetext')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_infos');
    }
}
