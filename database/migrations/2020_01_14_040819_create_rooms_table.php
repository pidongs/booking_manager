<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->unsignedInteger('ordernumber')->default(0);
            $table->text('description');
            $table->unsignedInteger('quantity')->default(0);
            $table->decimal('price', 10, 2)->default('0.00');
            $table->decimal('price_per_guest', 10, 2)->default('0.00');
            $table->text('safebox')->nullable();
            $table->unsignedInteger('guests')->default(0);
            $table->boolean('bookable')->default(0);
            $table->text('photourl')->nullable();
            $table->text('pageurl')->nullable();
            $table->string('rate_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('propertyId')->nullable();
            $table->string('roomId')->nullable();
            $table->string('rateId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
