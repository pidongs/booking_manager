<?php

use App\InvoiceInfo;
use App\PaymentInfo;
use App\PropertyInfo;
use App\Room;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create();
        //factory(Room::class, 10)->create();
        //factory(InvoiceInfo::class, 1)->create();
        //factory(PropertyInfo::class, 1)->create();
        //factory(PaymentInfo::class, 1)->create();
    }
}
