<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'type' => 'Room',
        'ordernumber' => rand(0, 10),
        'description' => $faker->sentence(rand(1, 3)),
        'guests' => rand(0, 10),
        'quantity' => 5,
        'price' => rand(1000.00, 5000.00),
        'price_per_guest' => rand(500.00, 1000.00),
        'bookable' => rand(0, 1),
        'photourl' => $faker->url(),
        'pageurl' => $faker->url(),
    ];
});
