<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PaymentInfo;
use Faker\Generator as Faker;

$factory->define(PaymentInfo::class, function (Faker $faker) {
    return [
        'requireprepay' => rand(0, 1),
        'prepaymore7d' => rand(0, 10),
        'prepayless7d' => rand(0, 10),
        'prepay' => $faker->sentence(rand(1, 2)),
        'prepay_success' => $faker->sentence(rand(1, 2)),
        'prepay_fail' => $faker->sentence(rand(1, 2)),
    ];
});
