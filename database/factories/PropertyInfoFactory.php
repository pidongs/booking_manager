<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PropertyInfo;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(PropertyInfo::class, function (Faker $faker) {
    return [
        'title' => $faker->text,
        'phone' => $faker->phoneNumber,
        'requirecc' => rand(0, 1),
        'type' => 'Resort',
        'email' => $faker->unique()->safeEmail,
        'logourl' => $faker->url,
        'country' => 'PH',
        'city' => 'Manila',
        'address' => $faker->address,
        'postalcode' => rand(1000,3000),
        'tandc' => $faker->url,
        'mindaysstay' => rand(1, 5),
        'otherinfo' => $faker->text,
        'checkin' => Carbon::parse('14:00:00')->format('H:i'),
        'checkout' => Carbon::parse('00:00:00')->format('H:i'),
    ];
});
