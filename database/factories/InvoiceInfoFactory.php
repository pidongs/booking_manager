<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InvoiceInfo;
use Faker\Generator as Faker;

$factory->define(InvoiceInfo::class, function (Faker $faker) {
    return [
        'logourl' => $faker->url(),
        'vat' => rand(0, 50),
        'invoiceprefix' => 'INV',
        'currency' => 'PHP',
        'companydetails' => $faker->sentence(rand(1, 2)),
        'description' => $faker->sentence(rand(1, 2)),
        'sendinvoice' => rand(0, 1),
        'invoicetext' => $faker->text,
        'sendapproval' => rand(0, 1),
        'approvaltext' => $faker->text,
        'invoicefooter' => $faker->text,
        'signaturetext' => $faker->text,
    ];
});
