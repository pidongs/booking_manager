<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register | Booking Manager</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('backend_vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend_vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{ asset('backend_css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{ asset('backend_images/favicon.png') }}" />

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-6 m-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo">
                    <img src="{{ asset('backend_images/logo.svg') }}">
                </div>
                <h4>New here?</h4>
                <h6 class="font-weight-light">Signing up is easy. It only takes a few steps</h6>
                <br>
                <form class="pt-3" id="register-form" method="POST" action="{{ route('register') }}">
                    @csrf
                    <p class="card-description"> Property Info </p>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="type">Type</label>
                            <select class="form-control" name="type" id="type">
                                <option value="Apartments">Apartments</option>
                                <option value="Hostel">Hostel</option>
                                <option value="Hotel">Hotel</option>
                                <option value="GuestHouse">Guest House</option>
                                <option value="Dormitory">Dormitory</option>
                                <option value="Rental">Rental</option>
                                <option value="Resort">Resort</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_name">Property Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control {{ $errors->has('property_name') ? 'is-invalid' : '' }}" id="property_name" name="property_name" placeholder="Property Name" value="{{ old('property_name') }}">
                            @if($errors->has('property_name'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('property_name') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_desc">Property Description <span class="text-danger">*</span></label>
                            <textarea type="text" class="form-control {{ $errors->has('property_desc') ? 'is-invalid' : '' }}" id="property_desc" name="property_desc" maxlength="255">{{ old('property_desc') }}</textarea>
                            @if($errors->has('property_desc'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('property_desc') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="company_details">Company Details <span class="text-danger">*</span></label>
                            <textarea type="text" class="form-control {{ $errors->has('company_details') ? 'is-invalid' : '' }}" id="company_details" name="company_details" maxlength="255">{{ old('company_details') }}</textarea>
                            @if($errors->has('company_details'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('company_details') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_telnum">Property Telephone Number <span class="text-danger">*</span></label>
                            <input type="text" class="form-control {{ $errors->has('property_telnum') ? 'is-invalid' : '' }}" id="property_telnum" name="property_telnum" placeholder="(+63)9123456789" value="{{ old('property_telnum') }}" data-inputmask-alias="(+99)9999999999">
                            @if($errors->has('property_telnum'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('property_telnum') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="website">Website</label>
                            <input type="text" class="form-control" id="website" name="website" placeholder="https://www.mywebsite.com" value="{{ old('website') }}">
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <p class="card-description"> Location Info </p>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_address">Property Address <span class="text-danger">*</span></label>
                            <textarea type="text" class="form-control {{ $errors->has('property_address') ? 'is-invalid' : '' }}" id="property_address" name="property_address" maxlength="255">{{ old('property_address') }}</textarea>
                            @if($errors->has('property_address'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('property_address') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="postal_code">Postal Code <span class="text-danger">*</span></label>
                            <input type="number" class="form-control {{ $errors->has('postal_code') ? 'is-invalid' : '' }}" id="postal_code" name="postal_code" placeholder="12345" value="{{ old('postal_code') }}">
                            @if($errors->has('postal_code'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('postal_code') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_city">Property City <span class="text-danger">*</span></label>
                            <input type="text" class="form-control {{ $errors->has('property_city') ? 'is-invalid' : '' }}" id="property_city" name="property_city" placeholder="Property City" value="{{ old('property_city') }}">
                            @if($errors->has('property_city'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('property_city') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="property_country">Property Country <span class="text-danger">*</span></label>
                            <select class="form-control" id="property_country" name="property_country">
                                <option value="AF">Afghanistan</option>
                                <option value="AX">Åland Islands</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia, Plurinational State of</option>
                                <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="BN">Brunei Darussalam</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CA">Canada</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos (Keeling) Islands</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo</option>
                                <option value="CD">Congo, the Democratic Republic of the</option>
                                <option value="CK">Cook Islands</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Côte d'Ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CW">Curaçao</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands (Malvinas)</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Southern Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GG">Guernsey</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard Island and McDonald Islands</option>
                                <option value="VA">Holy See (Vatican City State)</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran, Islamic Republic of</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IM">Isle of Man</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JE">Jersey</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KP">Korea, Democratic People's Republic of</option>
                                <option value="KR">Korea, Republic of</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao People's Democratic Republic</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macao</option>
                                <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia, Federated States of</option>
                                <option value="MD">Moldova, Republic of</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="ME">Montenegro</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanmar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NL">Netherlands</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PS">Palestinian Territory, Occupied</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH" selected>Philippines</option>
                                <option value="PN">Pitcairn</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Réunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="BL">Saint Barthélemy</option>
                                <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                <option value="KN">Saint Kitts and Nevis</option>
                                <option value="SO">Somalia</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                <option value="MF">Saint Martin (French part)</option>
                                <option value="ES">Spain</option>
                                <option value="PM">Saint Pierre and Miquelon</option>
                                <option value="SD">Sudan</option>
                                <option value="VC">Saint Vincent and the Grenadines</option>
                                <option value="SJ">Svalbard and Jan Mayen</option>
                                <option value="WS">Samoa</option>
                                <option value="SE">Sweden</option>
                                <option value="SM">San Marino</option>
                                <option value="SY">Syrian Arab Republic</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="TH">Thailand</option>
                                <option value="SN">Senegal</option>
                                <option value="TG">Togo</option>
                                <option value="RS">Serbia</option>
                                <option value="TO">Tonga</option>
                                <option value="SC">Seychelles</option>
                                <option value="TN">Tunisia</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="SG">Singapore</option>
                                <option value="TV">Tuvalu</option>
                                <option value="SX">Sint Maarten (Dutch part)</option>
                                <option value="UA">Ukraine</option>
                                <option value="SK">Slovakia</option>
                                <option value="GB">United Kingdom</option>
                                <option value="SI">Slovenia</option>
                                <option value="UM">United States Minor Outlying Islands</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="ZA">South Africa</option>
                                <option value="VE">Venezuela, Bolivarian Republic of</option>
                                <option value="SS">South Sudan</option>
                                <option value="VG">Virgin Islands, British</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="WF">Wallis and Futuna</option>
                                <option value="SR">Suriname</option>
                                <option value="YE">Yemen</option>
                                <option value="SZ">Swaziland</option>
                                <option value="ZW">Zimbabwe</option>
                                <option value="CH">Switzerland</option>
                                <option value="TW">Taiwan, Province of China</option>
                                <option value="TZ">Tanzania, United Republic of</option>
                                <option value="TL">Timor-Leste</option>
                                <option value="TK">Tokelau</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TR">Turkey</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="UG">Uganda</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="US">United States</option>
                                <option value="UY">Uruguay</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VN">Viet Nam</option>
                                <option value="VI">Virgin Islands, U.S.</option>
                                <option value="EH">Western Sahara</option>
                                <option value="ZM">Zambia</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <p class="card-description"> Contact Data </p>
                    <br>
                    {{-- <div class="form-group">
                        <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" id="name" placeholder="Name" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div> --}}
                    <div class="form-group">
                        <label for="property_email">Valid Property Email <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg @error('email') is-invalid @enderror" id="email" placeholder="labros@booking.com" name="email" value="{{ old('email') }}" autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Login Password <span class="text-danger">*</span></label>
                        <input type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password" autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        @if ($errors->has('g-recaptcha-response'))
                            <br><br>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6LdNvtcUAAAAACDE4dReQ5e7mgMtVoFm3WN0yCjO"></div>
                    </div>

                    {{-- <div class="form-group">
                        <input type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="password-confirm" placeholder="Confirm Password" name="password_confirmation" autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div> --}}
                    <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="tc" id="tc" required> Terms and Conditions <i class="input-helper"></i><span class="text-danger">*</span></label>
                        <label id="tc-error" class="error invalid-feedback" for="tc" style="display: none;"></label>
                    </div>
                    <br>
                    <div class="mt-4">
                        <input type="submit" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" value="SIGN UP">
                    </div>
                    <div class="text-center mt-4 font-weight-light"> Already have an account? <a href="{{ route('login') }}" class="text-primary">Sign In</a>
                    </div>
                </form>
              </div>
            </div>
            {{-- <div class="col-lg-4 mr-auto">
              <img src="{{ asset('backend_images/hotelclipart.png') }}" style="width: 600px;">
            </div> --}}
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('backend_vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <script src="{{ asset('backend_vendors/inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('backend_js/inputmask.js') }}"></script>
    <script src="{{ asset('backend_vendors/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- inject:js -->
    <script src="{{ asset('backend_js/off-canvas.js') }} "></script>
    <script src="{{ asset('backend_js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('backend_js/misc.js') }}"></script>
    <!-- endinject -->

    <script>
    $("#register-form").validate({
      rules: {
        tc: "required"
      },
      messages: {
        tc: "You must accept the Terms and Condition agreement!"
      },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
    });
    </script>
  </body>
</html>
