<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav ps ps--active-y">

  <li class="nav-item {{ (Request::is('dashboard') ? 'active' : '') }}">
      <a class="nav-link" data-toggle="collapse" href="#dashboard" aria-expanded="false" aria-controls="channels">
        <span class="menu-title">Dashboard</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-home menu-icon"></i>
      </a>
      <div class="collapse {{ (Request::is('dashboard') ? 'show' : '') }}" id="dashboard">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ url('/todayAvailable') }}">Today's Available</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ url('/tomorrowAvailable') }}">Tomorrow's Available</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item {{ (Request::is('newBooking') || Request::is('createNewBooking/*') || Request::is('bookings/*') || Request::is('todayCheckin') || Request::is('todayCheckout') || Request::is('tomorrowCheckin') || Request::is('tomorrowCheckout') ? 'active' : '') }}">
      <a class="nav-link" data-toggle="collapse" href="#bookings" aria-expanded="false" aria-controls="bookings">
        <span class="menu-title">Bookings</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-book-open-variant menu-icon"></i>
      </a>
      <div class="collapse {{ (Request::is('newBooking') || Request::is('createNewBooking/*') || Request::is('bookings/*') || Request::is('todayCheckin') || Request::is('todayCheckout') || Request::is('tomorrowCheckin') || Request::is('tomorrowCheckout') ? 'show' : '') }}" id="bookings">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link {{ (Request::is('newBooking') ? 'active' : '') }}" href="{{ route('bookings.create') }}">New booking</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('bookings.calendar') }}">Calendar</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('bookings.todayCheckin') }}">Today's Check-in</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('bookings.todayCheckout') }}">Today's Check-out</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('bookings.tomorrowCheckin') }}">Tomorrow's Check-in</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('bookings.tomorrowCheckout') }}">Tomorrow's Check-out</a></li>
        </ul>
      </div>
    </li>

     <li class="nav-item {{ (Request::is('availabilityCalendar/*') || Request::is('set/*') ? 'active' : '') }}">
      <a class="nav-link" data-toggle="collapse" href="#availability" aria-expanded="false" aria-controls="channels">
        <span class="menu-title">Availability Calendar</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-calendar menu-icon"></i>
      </a>
      <div class="collapse {{ (Request::is('availabilityCalendar/*') || Request::is('set/*') ? 'show' : '') }}" id="availability">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link {{ (Request::is('availabilityCalendar/*') ? 'active' : '') }}" href="{{ route('availabilityCalendar') }}">Calendar</a></li>
        <li class="nav-item"> <a class="nav-link {{ Request::is('set/*') ? 'active' : '' }}" href="{{ route('setNewAvailabilityRates') }}">Set Availability & Rates</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#channels" aria-expanded="false" aria-controls="channels">
        <span class="menu-title">Channels</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-radar menu-icon"></i>
      </a>
      <div class="collapse" id="channels">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('expedia') }}">Expedia</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item {{ (Request::is('createRoom') || Request::is('createManager') || Request::is('propertyInfo/*') || Request::is('invoiceInfo/*') || Request::is('paymentInfo/*') || Request::is('rooms/*') || Request::is('managers/*') || Request::is('logs/*') ? 'active' : '') }}">
      <a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="false" aria-controls="settings">
        <span class="menu-title">Settings</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-settings menu-icon"></i>
      </a>
      <div class="collapse {{ (Request::is('createRoom') || Request::is('createManager') || Request::is('propertyInfo/*') || Request::is('invoiceInfo/*') || Request::is('paymentInfo/*') || Request::is('rooms/*') || Request::is('managers/*') || Request::is('logs/*') ? 'show' : '') }}" id="settings">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link {{ (Request::is('propertyInfo/*') ? 'active' : '') }}" href="{{ route('editPropertyInfo', ['id' => auth()->user()->property_info_id]) }}">Property Info</a></li>
          <li class="nav-item"> <a class="nav-link {{ (Request::is('invoiceInfo/*') ? 'active' : '') }}" href="{{ route('editInvoiceInfo', ['id' => auth()->user()->invoice_info_id]) }}">Invoice Info</a></li>
          <li class="nav-item"> <a class="nav-link {{ (Request::is('paymentInfo/*') ? 'active' : '') }}" href="{{ route('editPaymentInfo', ['id' => auth()->user()->payment_info_id]) }}">Payment Info</a></li>
          <li class="nav-item"> <a class="nav-link {{ (Request::is('rooms/*') || Request::is('createRoom') ? 'active' : '') }}" href="{{ route('rooms.index') }}">Rooms</a></li>
          <li class="nav-item"> <a class="nav-link {{ (Request::is('managers/*') || Request::is('createManager') ? 'active' : '') }}" href="{{ route('managers.index') }}">Managers</a></li>
          <li class="nav-item"> <a class="nav-link {{ (Request::is('logs/*') ? 'active' : '') }}" href="{{ route('logs.index') }}">Logs</a></li>
        </ul>
      </div>
    </li>

     <li class="nav-item sidebar-actions">
      <span class="nav-link">
        <div class="border-bottom">
        </div>
      </span>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
        <span class="menu-title">Sign Out</span>
        <i class="mdi mdi-power menu-icon"></i>
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
  </ul>
</nav>
