@extends('layouts.master')

@push('head')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush

@section('title', 'New Booking | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-home-map-marker"></i>
        </span> New Booking </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Bookings</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Booking</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <!--FORM-->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('layouts._messages')
                    <form class="form-sample" action="{{ route('bookings.store') }}" method="POST">
                        @include('admin.settings-bookings._form', ['action' => 'add'])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(function() {
    var r = "{{ $r }}";

    if(r == 1){
        swal({
            text: "No Rooms yet.",
            icon: 'warning',
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "Add now?",
                    value: true,
                    visible: true,
                    className: "btn btn-primary",
                    closeModal: true
                }
            }
        }).then(val => {
            if (val) {
                location.replace("/createRoom");
            }
            else{
                location.replace("/dashboard");
            }
        })
    }

    $('#checkin').val(moment().format('MM/DD/YYYY'));
    $('#checkout').val(moment().add(1, 'day').format('MM/DD/YYYY'));

    $('#daterange').daterangepicker({
        "autoApply": true,
        "startDate": moment().format('MM/DD/YYYY'),
        "endDate": moment().add(1, 'day').format('MM/DD/YYYY'),
        "minDate": moment().format('MM/DD/YYYY'),
    }, function(start, end) {
        $('#nights').val(end.diff(start, 'days'));
        $('#checkin').val(start.format('MM/DD/YYYY'));
        $('#checkout').val(end.format('MM/DD/YYYY'));
        getPriceTotal();
    });

    getPriceTotal();

    $('#room').change(function(){
        getPriceTotal();
    });

    $('#guests').keyup(function(){
        getPriceTotal();
    });

    function getPriceTotal (){
        $.ajax({
            url: "{{ route('getPriceTotal') }}",
            method: 'post',
            data: {
                nights: $('#nights').val(),
                guests: $('#guests').val(),
                room_id: $('#room').val()
            },
            success: function(result){
                $('#price').val(result.price);
                $('#total').val(result.total);
            }
        });
    }
});
</script>
@endpush
