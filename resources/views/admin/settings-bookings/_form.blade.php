@csrf
<input type="hidden" value="{{ auth()->user()->id }}" id="user_id" name="user_id">
<!--BUTTON SAVE-->
<div class="pull-right">
    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
</div>
<!--FORM-->
<p class="card-description"> Booking Details </p>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
        <label for="daterange">Check-in / Check-out</label>
        <input type="hidden" id="checkin" name="checkin">
        <input type="hidden" id="checkout" name="checkout">
        <input type="text" class="form-control" id="daterange" name="daterange" readonly>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="room">Room Type</label> <!--dropdown-->
            <select class="form-control" id="room" name="room">
                @foreach($rooms as $room)
                    @if(!empty($data))
                        <option value="{{ $room->id }}" {{ ($room->id==$data) ? 'selected' : '' }}>{{ $room->name }}</option>
                    @else
                        <option value="{{ $room->id }}" {{ ($action=='edit') ? (($room->id==$booking->room) ? 'selected' : '') : '' }}>{{ $room->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="nights">Nights</label>
        <input type="number" class="form-control" id="nights" name="nights" placeholder="0" value="{{ ($action=='edit') ? ($booking->nights) : 1 }}" readonly>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
        <label for="guests">Guests</label>
        <input type="number" class="form-control {{ $errors->has('guests') ? 'is-invalid' : '' }}" id="guests" name="guests" placeholder="0" value="{{ ($action=='edit') ? (old('guests', $booking->guests)) : (old('guests', 1)) }}">
        @if($errors->has('guests'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('guests') }}</strong>
            </div>
        @endif
        </div>
    </div>
    {{-- <div class="col-md-4">
        @if($action=='edit')
        <div class="form-group">
            <label for="guests">Status</label>
            <select class="form-control" id="status" name="status">
            <option value="New" {{ ($action=='edit') ? (($booking->status=='New') ? 'selected' : '') : '' }}>New</option>
                <option value="Cancelled" {{ ($action=='edit') ? (($booking->status=='Cancelled') ? 'selected' : '') : '' }}>Cancelled</option>
                <option value="Checkin" {{ ($action=='edit') ? (($booking->status=='Checkin') ? 'selected' : '') : '' }}>Checkin</option>
                <option value="Checkout" {{ ($action=='edit') ? (($booking->status=='Checkout') ? 'selected' : '') : '' }}>Checkout</option>
                <option value="Paid" {{ ($action=='edit') ? (($booking->status=='Paid') ? 'selected' : '') : '' }}>Paid</option>
                <option value="PaidCC" {{ ($action=='edit') ? (($booking->status=='PaidCC') ? 'selected' : '') : '' }}>PaidCC</option>
            </select>
        </div>
        @endif
    </div> --}}
    <div class="col-md-4">
    </div>
</div>

<p class="card-description"> Customer Details </p>
    <div class="row">
    <div class="col-md-4">
        <div class="form-group">
        <label for="customer_name">Name <span class="text-danger">*</span></label>
        <input type="text" class="form-control {{ $errors->has('customer_name') ? 'is-invalid' : '' }}" id="customer_name" name="customer_name" placeholder="Name" value="{{ ($action=='edit') ? (old('customer_name', $booking->customer_name)) : (old('customer_name')) }}">
        @if($errors->has('customer_name'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('customer_name') }}</strong>
            </div>
        @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="customer_address">Address</label>
        <input type="text" class="form-control" id="customer_address" name="customer_address" placeholder="Address" value="{{ ($action=='edit') ? (old('customer_address', $booking->customer_address)) : (old('customer_address')) }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="customer_country">Country</label>
        <select class="form-control" id="customer_country" name="customer_country">
            <option value="AF" {{ ($action=='edit') ? (($booking->customer_country=='AF') ? 'selected' : '') : '' }}>Afghanistan</option>
            <option value="AX" {{ ($action=='edit') ? (($booking->customer_country=='AX') ? 'selected' : '') : '' }}>Åland Islands</option>
            <option value="AL" {{ ($action=='edit') ? (($booking->customer_country=='AL') ? 'selected' : '') : '' }}>Albania</option>
            <option value="DZ" {{ ($action=='edit') ? (($booking->customer_country=='DZ') ? 'selected' : '') : '' }}>Algeria</option>
            <option value="AS" {{ ($action=='edit') ? (($booking->customer_country=='AS') ? 'selected' : '') : '' }}>American Samoa</option>
            <option value="AD" {{ ($action=='edit') ? (($booking->customer_country=='AD') ? 'selected' : '') : '' }}>Andorra</option>
            <option value="AO" {{ ($action=='edit') ? (($booking->customer_country=='AO') ? 'selected' : '') : '' }}>Angola</option>
            <option value="AI" {{ ($action=='edit') ? (($booking->customer_country=='AI') ? 'selected' : '') : '' }}>Anguilla</option>
            <option value="AQ" {{ ($action=='edit') ? (($booking->customer_country=='AQ') ? 'selected' : '') : '' }}>Antarctica</option>
            <option value="AG" {{ ($action=='edit') ? (($booking->customer_country=='AG') ? 'selected' : '') : '' }}>Antigua and Barbuda</option>
            <option value="AR" {{ ($action=='edit') ? (($booking->customer_country=='AR') ? 'selected' : '') : '' }}>Argentina</option>
            <option value="AM" {{ ($action=='edit') ? (($booking->customer_country=='AM') ? 'selected' : '') : '' }}>Armenia</option>
            <option value="AW" {{ ($action=='edit') ? (($booking->customer_country=='AW') ? 'selected' : '') : '' }}>Aruba</option>
            <option value="AU" {{ ($action=='edit') ? (($booking->customer_country=='AU') ? 'selected' : '') : '' }}>Australia</option>
            <option value="AT" {{ ($action=='edit') ? (($booking->customer_country=='AT') ? 'selected' : '') : '' }}>Austria</option>
            <option value="AZ" {{ ($action=='edit') ? (($booking->customer_country=='AZ') ? 'selected' : '') : '' }}>Azerbaijan</option>
            <option value="BS" {{ ($action=='edit') ? (($booking->customer_country=='BS') ? 'selected' : '') : '' }}>Bahamas</option>
            <option value="BH" {{ ($action=='edit') ? (($booking->customer_country=='BH') ? 'selected' : '') : '' }}>Bahrain</option>
            <option value="BD" {{ ($action=='edit') ? (($booking->customer_country=='BD') ? 'selected' : '') : '' }}>Bangladesh</option>
            <option value="BB" {{ ($action=='edit') ? (($booking->customer_country=='BB') ? 'selected' : '') : '' }}>Barbados</option>
            <option value="BY" {{ ($action=='edit') ? (($booking->customer_country=='BY') ? 'selected' : '') : '' }}>Belarus</option>
            <option value="BE" {{ ($action=='edit') ? (($booking->customer_country=='BE') ? 'selected' : '') : '' }}>Belgium</option>
            <option value="BZ" {{ ($action=='edit') ? (($booking->customer_country=='BZ') ? 'selected' : '') : '' }}>Belize</option>
            <option value="BJ" {{ ($action=='edit') ? (($booking->customer_country=='BJ') ? 'selected' : '') : '' }}>Benin</option>
            <option value="BM" {{ ($action=='edit') ? (($booking->customer_country=='BM') ? 'selected' : '') : '' }}>Bermuda</option>
            <option value="BT" {{ ($action=='edit') ? (($booking->customer_country=='BT') ? 'selected' : '') : '' }}>Bhutan</option>
            <option value="BO" {{ ($action=='edit') ? (($booking->customer_country=='BO') ? 'selected' : '') : '' }}>Bolivia, Plurinational State of</option>
            <option value="BQ" {{ ($action=='edit') ? (($booking->customer_country=='BQ') ? 'selected' : '') : '' }}>Bonaire, Sint Eustatius and Saba</option>
            <option value="BA" {{ ($action=='edit') ? (($booking->customer_country=='BA') ? 'selected' : '') : '' }}>Bosnia and Herzegovina</option>
            <option value="BW" {{ ($action=='edit') ? (($booking->customer_country=='BW') ? 'selected' : '') : '' }}>Botswana</option>
            <option value="BV" {{ ($action=='edit') ? (($booking->customer_country=='BV') ? 'selected' : '') : '' }}>Bouvet Island</option>
            <option value="BR" {{ ($action=='edit') ? (($booking->customer_country=='BR') ? 'selected' : '') : '' }}>Brazil</option>
            <option value="IO" {{ ($action=='edit') ? (($booking->customer_country=='IO') ? 'selected' : '') : '' }}>British Indian Ocean Territory</option>
            <option value="BN" {{ ($action=='edit') ? (($booking->customer_country=='BN') ? 'selected' : '') : '' }}>Brunei Darussalam</option>
            <option value="BG" {{ ($action=='edit') ? (($booking->customer_country=='BG') ? 'selected' : '') : '' }}>Bulgaria</option>
            <option value="BF" {{ ($action=='edit') ? (($booking->customer_country=='BF') ? 'selected' : '') : '' }}>Burkina Faso</option>
            <option value="BI" {{ ($action=='edit') ? (($booking->customer_country=='BI') ? 'selected' : '') : '' }}>Burundi</option>
            <option value="KH" {{ ($action=='edit') ? (($booking->customer_country=='KH') ? 'selected' : '') : '' }}>Cambodia</option>
            <option value="CM" {{ ($action=='edit') ? (($booking->customer_country=='CM') ? 'selected' : '') : '' }}>Cameroon</option>
            <option value="CA" {{ ($action=='edit') ? (($booking->customer_country=='CA') ? 'selected' : '') : '' }}>Canada</option>
            <option value="CV" {{ ($action=='edit') ? (($booking->customer_country=='CV') ? 'selected' : '') : '' }}>Cape Verde</option>
            <option value="KY" {{ ($action=='edit') ? (($booking->customer_country=='KY') ? 'selected' : '') : '' }}>Cayman Islands</option>
            <option value="CF" {{ ($action=='edit') ? (($booking->customer_country=='CF') ? 'selected' : '') : '' }}>Central African Republic</option>
            <option value="TD" {{ ($action=='edit') ? (($booking->customer_country=='TD') ? 'selected' : '') : '' }}>Chad</option>
            <option value="CL" {{ ($action=='edit') ? (($booking->customer_country=='CL') ? 'selected' : '') : '' }}>Chile</option>
            <option value="CN" {{ ($action=='edit') ? (($booking->customer_country=='CN') ? 'selected' : '') : '' }}>China</option>
            <option value="CX" {{ ($action=='edit') ? (($booking->customer_country=='CX') ? 'selected' : '') : '' }}>Christmas Island</option>
            <option value="CC" {{ ($action=='edit') ? (($booking->customer_country=='CC') ? 'selected' : '') : '' }}>Cocos (Keeling) Islands</option>
            <option value="CO" {{ ($action=='edit') ? (($booking->customer_country=='CO') ? 'selected' : '') : '' }}>Colombia</option>
            <option value="KM" {{ ($action=='edit') ? (($booking->customer_country=='KM') ? 'selected' : '') : '' }}>Comoros</option>
            <option value="CG" {{ ($action=='edit') ? (($booking->customer_country=='CG') ? 'selected' : '') : '' }}>Congo</option>
            <option value="CD" {{ ($action=='edit') ? (($booking->customer_country=='CD') ? 'selected' : '') : '' }}>Congo, the Democratic Republic of the</option>
            <option value="CK" {{ ($action=='edit') ? (($booking->customer_country=='CK') ? 'selected' : '') : '' }}>Cook Islands</option>
            <option value="CR" {{ ($action=='edit') ? (($booking->customer_country=='CR') ? 'selected' : '') : '' }}>Costa Rica</option>
            <option value="CI" {{ ($action=='edit') ? (($booking->customer_country=='CI') ? 'selected' : '') : '' }}>Côte d'Ivoire</option>
            <option value="HR" {{ ($action=='edit') ? (($booking->customer_country=='HR') ? 'selected' : '') : '' }}>Croatia</option>
            <option value="CU" {{ ($action=='edit') ? (($booking->customer_country=='CU') ? 'selected' : '') : '' }}>Cuba</option>
            <option value="CW" {{ ($action=='edit') ? (($booking->customer_country=='CW') ? 'selected' : '') : '' }}>Curaçao</option>
            <option value="CY" {{ ($action=='edit') ? (($booking->customer_country=='CY') ? 'selected' : '') : '' }}>Cyprus</option>
            <option value="CZ" {{ ($action=='edit') ? (($booking->customer_country=='CZ') ? 'selected' : '') : '' }}>Czech Republic</option>
            <option value="DK" {{ ($action=='edit') ? (($booking->customer_country=='DK') ? 'selected' : '') : '' }}>Denmark</option>
            <option value="DJ" {{ ($action=='edit') ? (($booking->customer_country=='DJ') ? 'selected' : '') : '' }}>Djibouti</option>
            <option value="DM" {{ ($action=='edit') ? (($booking->customer_country=='DM') ? 'selected' : '') : '' }}>Dominica</option>
            <option value="DO" {{ ($action=='edit') ? (($booking->customer_country=='DO') ? 'selected' : '') : '' }}>Dominican Republic</option>
            <option value="EC" {{ ($action=='edit') ? (($booking->customer_country=='EC') ? 'selected' : '') : '' }}>Ecuador</option>
            <option value="EG" {{ ($action=='edit') ? (($booking->customer_country=='EG') ? 'selected' : '') : '' }}>Egypt</option>
            <option value="SV" {{ ($action=='edit') ? (($booking->customer_country=='SV') ? 'selected' : '') : '' }}>El Salvador</option>
            <option value="GQ" {{ ($action=='edit') ? (($booking->customer_country=='GQ') ? 'selected' : '') : '' }}>Equatorial Guinea</option>
            <option value="ER" {{ ($action=='edit') ? (($booking->customer_country=='ER') ? 'selected' : '') : '' }}>Eritrea</option>
            <option value="EE" {{ ($action=='edit') ? (($booking->customer_country=='EE') ? 'selected' : '') : '' }}>Estonia</option>
            <option value="ET" {{ ($action=='edit') ? (($booking->customer_country=='ET') ? 'selected' : '') : '' }}>Ethiopia</option>
            <option value="FK" {{ ($action=='edit') ? (($booking->customer_country=='FK') ? 'selected' : '') : '' }}>Falkland Islands (Malvinas)</option>
            <option value="FO" {{ ($action=='edit') ? (($booking->customer_country=='FO') ? 'selected' : '') : '' }}>Faroe Islands</option>
            <option value="FJ" {{ ($action=='edit') ? (($booking->customer_country=='FJ') ? 'selected' : '') : '' }}>Fiji</option>
            <option value="FI" {{ ($action=='edit') ? (($booking->customer_country=='FI') ? 'selected' : '') : '' }}>Finland</option>
            <option value="FR" {{ ($action=='edit') ? (($booking->customer_country=='FR') ? 'selected' : '') : '' }}>France</option>
            <option value="GF" {{ ($action=='edit') ? (($booking->customer_country=='GF') ? 'selected' : '') : '' }}>French Guiana</option>
            <option value="PF" {{ ($action=='edit') ? (($booking->customer_country=='PF') ? 'selected' : '') : '' }}>French Polynesia</option>
            <option value="TF" {{ ($action=='edit') ? (($booking->customer_country=='TF') ? 'selected' : '') : '' }}>French Southern Territories</option>
            <option value="GA" {{ ($action=='edit') ? (($booking->customer_country=='GA') ? 'selected' : '') : '' }}>Gabon</option>
            <option value="GM" {{ ($action=='edit') ? (($booking->customer_country=='GM') ? 'selected' : '') : '' }}>Gambia</option>
            <option value="GE" {{ ($action=='edit') ? (($booking->customer_country=='GE') ? 'selected' : '') : '' }}>Georgia</option>
            <option value="DE" {{ ($action=='edit') ? (($booking->customer_country=='DE') ? 'selected' : '') : '' }}>Germany</option>
            <option value="GH" {{ ($action=='edit') ? (($booking->customer_country=='GH') ? 'selected' : '') : '' }}>Ghana</option>
            <option value="GI" {{ ($action=='edit') ? (($booking->customer_country=='GI') ? 'selected' : '') : '' }}>Gibraltar</option>
            <option value="GR" {{ ($action=='edit') ? (($booking->customer_country=='GR') ? 'selected' : '') : '' }}>Greece</option>
            <option value="GL" {{ ($action=='edit') ? (($booking->customer_country=='GL') ? 'selected' : '') : '' }}>Greenland</option>
            <option value="GD" {{ ($action=='edit') ? (($booking->customer_country=='GD') ? 'selected' : '') : '' }}>Grenada</option>
            <option value="GP" {{ ($action=='edit') ? (($booking->customer_country=='GP') ? 'selected' : '') : '' }}>Guadeloupe</option>
            <option value="GU" {{ ($action=='edit') ? (($booking->customer_country=='GU') ? 'selected' : '') : '' }}>Guam</option>
            <option value="GT" {{ ($action=='edit') ? (($booking->customer_country=='GT') ? 'selected' : '') : '' }}>Guatemala</option>
            <option value="GG" {{ ($action=='edit') ? (($booking->customer_country=='GG') ? 'selected' : '') : '' }}>Guernsey</option>
            <option value="GN" {{ ($action=='edit') ? (($booking->customer_country=='GN') ? 'selected' : '') : '' }}>Guinea</option>
            <option value="GW" {{ ($action=='edit') ? (($booking->customer_country=='GW') ? 'selected' : '') : '' }}>Guinea-Bissau</option>
            <option value="GY" {{ ($action=='edit') ? (($booking->customer_country=='GY') ? 'selected' : '') : '' }}>Guyana</option>
            <option value="HT" {{ ($action=='edit') ? (($booking->customer_country=='HT') ? 'selected' : '') : '' }}>Haiti</option>
            <option value="HM" {{ ($action=='edit') ? (($booking->customer_country=='HM') ? 'selected' : '') : '' }}>Heard Island and McDonald Islands</option>
            <option value="VA" {{ ($action=='edit') ? (($booking->customer_country=='VA') ? 'selected' : '') : '' }}>Holy See (Vatican City State)</option>
            <option value="HN" {{ ($action=='edit') ? (($booking->customer_country=='HN') ? 'selected' : '') : '' }}>Honduras</option>
            <option value="HK" {{ ($action=='edit') ? (($booking->customer_country=='HK') ? 'selected' : '') : '' }}>Hong Kong</option>
            <option value="HU" {{ ($action=='edit') ? (($booking->customer_country=='HU') ? 'selected' : '') : '' }}>Hungary</option>
            <option value="IS" {{ ($action=='edit') ? (($booking->customer_country=='IS') ? 'selected' : '') : '' }}>Iceland</option>
            <option value="IN" {{ ($action=='edit') ? (($booking->customer_country=='IN') ? 'selected' : '') : '' }}>India</option>
            <option value="ID" {{ ($action=='edit') ? (($booking->customer_country=='ID') ? 'selected' : '') : '' }}>Indonesia</option>
            <option value="IR" {{ ($action=='edit') ? (($booking->customer_country=='IR') ? 'selected' : '') : '' }}>Iran, Islamic Republic of</option>
            <option value="IQ" {{ ($action=='edit') ? (($booking->customer_country=='IQ') ? 'selected' : '') : '' }}>Iraq</option>
            <option value="IE" {{ ($action=='edit') ? (($booking->customer_country=='IE') ? 'selected' : '') : '' }}>Ireland</option>
            <option value="IM" {{ ($action=='edit') ? (($booking->customer_country=='IM') ? 'selected' : '') : '' }}>Isle of Man</option>
            <option value="IL" {{ ($action=='edit') ? (($booking->customer_country=='IL') ? 'selected' : '') : '' }}>Israel</option>
            <option value="IT" {{ ($action=='edit') ? (($booking->customer_country=='IT') ? 'selected' : '') : '' }}>Italy</option>
            <option value="JM" {{ ($action=='edit') ? (($booking->customer_country=='JM') ? 'selected' : '') : '' }}>Jamaica</option>
            <option value="JP" {{ ($action=='edit') ? (($booking->customer_country=='JP') ? 'selected' : '') : '' }}>Japan</option>
            <option value="JE" {{ ($action=='edit') ? (($booking->customer_country=='JE') ? 'selected' : '') : '' }}>Jersey</option>
            <option value="JO" {{ ($action=='edit') ? (($booking->customer_country=='JO') ? 'selected' : '') : '' }}>Jordan</option>
            <option value="KZ" {{ ($action=='edit') ? (($booking->customer_country=='KZ') ? 'selected' : '') : '' }}>Kazakhstan</option>
            <option value="KE" {{ ($action=='edit') ? (($booking->customer_country=='KE') ? 'selected' : '') : '' }}>Kenya</option>
            <option value="KI" {{ ($action=='edit') ? (($booking->customer_country=='KI') ? 'selected' : '') : '' }}>Kiribati</option>
            <option value="KP" {{ ($action=='edit') ? (($booking->customer_country=='KP') ? 'selected' : '') : '' }}>Korea, Democratic People's Republic of</option>
            <option value="KR" {{ ($action=='edit') ? (($booking->customer_country=='KR') ? 'selected' : '') : '' }}>Korea, Republic of</option>
            <option value="KW" {{ ($action=='edit') ? (($booking->customer_country=='KW') ? 'selected' : '') : '' }}>Kuwait</option>
            <option value="KG" {{ ($action=='edit') ? (($booking->customer_country=='KG') ? 'selected' : '') : '' }}>Kyrgyzstan</option>
            <option value="LA" {{ ($action=='edit') ? (($booking->customer_country=='LA') ? 'selected' : '') : '' }}>Lao People's Democratic Republic</option>
            <option value="LV" {{ ($action=='edit') ? (($booking->customer_country=='LV') ? 'selected' : '') : '' }}>Latvia</option>
            <option value="LB" {{ ($action=='edit') ? (($booking->customer_country=='LB') ? 'selected' : '') : '' }}>Lebanon</option>
            <option value="LS" {{ ($action=='edit') ? (($booking->customer_country=='LS') ? 'selected' : '') : '' }}>Lesotho</option>
            <option value="LR" {{ ($action=='edit') ? (($booking->customer_country=='LR') ? 'selected' : '') : '' }}>Liberia</option>
            <option value="LY" {{ ($action=='edit') ? (($booking->customer_country=='LY') ? 'selected' : '') : '' }}>Libya</option>
            <option value="LI" {{ ($action=='edit') ? (($booking->customer_country=='LI') ? 'selected' : '') : '' }}>Liechtenstein</option>
            <option value="LT" {{ ($action=='edit') ? (($booking->customer_country=='LT') ? 'selected' : '') : '' }}>Lithuania</option>
            <option value="LU" {{ ($action=='edit') ? (($booking->customer_country=='LU') ? 'selected' : '') : '' }}>Luxembourg</option>
            <option value="MO" {{ ($action=='edit') ? (($booking->customer_country=='MO') ? 'selected' : '') : '' }}>Macao</option>
            <option value="MK" {{ ($action=='edit') ? (($booking->customer_country=='MK') ? 'selected' : '') : '' }}>Macedonia, the former Yugoslav Republic of</option>
            <option value="MG" {{ ($action=='edit') ? (($booking->customer_country=='MG') ? 'selected' : '') : '' }}>Madagascar</option>
            <option value="MW" {{ ($action=='edit') ? (($booking->customer_country=='MW') ? 'selected' : '') : '' }}>Malawi</option>
            <option value="MY" {{ ($action=='edit') ? (($booking->customer_country=='MY') ? 'selected' : '') : '' }}>Malaysia</option>
            <option value="MV" {{ ($action=='edit') ? (($booking->customer_country=='MV') ? 'selected' : '') : '' }}>Maldives</option>
            <option value="ML" {{ ($action=='edit') ? (($booking->customer_country=='ML') ? 'selected' : '') : '' }}>Mali</option>
            <option value="MT" {{ ($action=='edit') ? (($booking->customer_country=='MT') ? 'selected' : '') : '' }}>Malta</option>
            <option value="MH" {{ ($action=='edit') ? (($booking->customer_country=='MH') ? 'selected' : '') : '' }}>Marshall Islands</option>
            <option value="MQ" {{ ($action=='edit') ? (($booking->customer_country=='MQ') ? 'selected' : '') : '' }}>Martinique</option>
            <option value="MR" {{ ($action=='edit') ? (($booking->customer_country=='MR') ? 'selected' : '') : '' }}>Mauritania</option>
            <option value="MU" {{ ($action=='edit') ? (($booking->customer_country=='MU') ? 'selected' : '') : '' }}>Mauritius</option>
            <option value="YT" {{ ($action=='edit') ? (($booking->customer_country=='YT') ? 'selected' : '') : '' }}>Mayotte</option>
            <option value="MX" {{ ($action=='edit') ? (($booking->customer_country=='MX') ? 'selected' : '') : '' }}>Mexico</option>
            <option value="FM" {{ ($action=='edit') ? (($booking->customer_country=='FM') ? 'selected' : '') : '' }}>Micronesia, Federated States of</option>
            <option value="MD" {{ ($action=='edit') ? (($booking->customer_country=='MD') ? 'selected' : '') : '' }}>Moldova, Republic of</option>
            <option value="MC" {{ ($action=='edit') ? (($booking->customer_country=='MC') ? 'selected' : '') : '' }}>Monaco</option>
            <option value="MN" {{ ($action=='edit') ? (($booking->customer_country=='MN') ? 'selected' : '') : '' }}>Mongolia</option>
            <option value="ME" {{ ($action=='edit') ? (($booking->customer_country=='ME') ? 'selected' : '') : '' }}>Montenegro</option>
            <option value="MS" {{ ($action=='edit') ? (($booking->customer_country=='MS') ? 'selected' : '') : '' }}>Montserrat</option>
            <option value="MA" {{ ($action=='edit') ? (($booking->customer_country=='MA') ? 'selected' : '') : '' }}>Morocco</option>
            <option value="MZ" {{ ($action=='edit') ? (($booking->customer_country=='MZ') ? 'selected' : '') : '' }}>Mozambique</option>
            <option value="MM" {{ ($action=='edit') ? (($booking->customer_country=='MM') ? 'selected' : '') : '' }}>Myanmar</option>
            <option value="NA" {{ ($action=='edit') ? (($booking->customer_country=='NA') ? 'selected' : '') : '' }}>Namibia</option>
            <option value="NR" {{ ($action=='edit') ? (($booking->customer_country=='NR') ? 'selected' : '') : '' }}>Nauru</option>
            <option value="NP" {{ ($action=='edit') ? (($booking->customer_country=='NP') ? 'selected' : '') : '' }}>Nepal</option>
            <option value="NL" {{ ($action=='edit') ? (($booking->customer_country=='NL') ? 'selected' : '') : '' }}>Netherlands</option>
            <option value="NC" {{ ($action=='edit') ? (($booking->customer_country=='NC') ? 'selected' : '') : '' }}>New Caledonia</option>
            <option value="NZ" {{ ($action=='edit') ? (($booking->customer_country=='NZ') ? 'selected' : '') : '' }}>New Zealand</option>
            <option value="NI" {{ ($action=='edit') ? (($booking->customer_country=='NI') ? 'selected' : '') : '' }}>Nicaragua</option>
            <option value="NE" {{ ($action=='edit') ? (($booking->customer_country=='NE') ? 'selected' : '') : '' }}>Niger</option>
            <option value="NG" {{ ($action=='edit') ? (($booking->customer_country=='NG') ? 'selected' : '') : '' }}>Nigeria</option>
            <option value="NU" {{ ($action=='edit') ? (($booking->customer_country=='NU') ? 'selected' : '') : '' }}>Niue</option>
            <option value="NF" {{ ($action=='edit') ? (($booking->customer_country=='NF') ? 'selected' : '') : '' }}>Norfolk Island</option>
            <option value="MP" {{ ($action=='edit') ? (($booking->customer_country=='MP') ? 'selected' : '') : '' }}>Northern Mariana Islands</option>
            <option value="NO" {{ ($action=='edit') ? (($booking->customer_country=='NO') ? 'selected' : '') : '' }}>Norway</option>
            <option value="OM" {{ ($action=='edit') ? (($booking->customer_country=='OM') ? 'selected' : '') : '' }}>Oman</option>
            <option value="PK" {{ ($action=='edit') ? (($booking->customer_country=='PK') ? 'selected' : '') : '' }}>Pakistan</option>
            <option value="PW" {{ ($action=='edit') ? (($booking->customer_country=='PW') ? 'selected' : '') : '' }}>Palau</option>
            <option value="PS" {{ ($action=='edit') ? (($booking->customer_country=='PS') ? 'selected' : '') : '' }}>Palestinian Territory, Occupied</option>
            <option value="PA" {{ ($action=='edit') ? (($booking->customer_country=='PA') ? 'selected' : '') : '' }}>Panama</option>
            <option value="PG" {{ ($action=='edit') ? (($booking->customer_country=='PG') ? 'selected' : '') : '' }}>Papua New Guinea</option>
            <option value="PY" {{ ($action=='edit') ? (($booking->customer_country=='PY') ? 'selected' : '') : '' }}>Paraguay</option>
            <option value="PE" {{ ($action=='edit') ? (($booking->customer_country=='PE') ? 'selected' : '') : '' }}>Peru</option>
            <option value="PH" {{ ($action=='edit') ? (($booking->customer_country=='PH') ? 'selected' : '') : 'selected' }}>Philippines</option>
            <option value="PN" {{ ($action=='edit') ? (($booking->customer_country=='PN') ? 'selected' : '') : '' }}>Pitcairn</option>
            <option value="PL" {{ ($action=='edit') ? (($booking->customer_country=='PL') ? 'selected' : '') : '' }}>Poland</option>
            <option value="PT" {{ ($action=='edit') ? (($booking->customer_country=='PT') ? 'selected' : '') : '' }}>Portugal</option>
            <option value="PR" {{ ($action=='edit') ? (($booking->customer_country=='PR') ? 'selected' : '') : '' }}>Puerto Rico</option>
            <option value="QA" {{ ($action=='edit') ? (($booking->customer_country=='QA') ? 'selected' : '') : '' }}>Qatar</option>
            <option value="RE" {{ ($action=='edit') ? (($booking->customer_country=='RE') ? 'selected' : '') : '' }}>Réunion</option>
            <option value="RO" {{ ($action=='edit') ? (($booking->customer_country=='RO') ? 'selected' : '') : '' }}>Romania</option>
            <option value="RU" {{ ($action=='edit') ? (($booking->customer_country=='RU') ? 'selected' : '') : '' }}>Russian Federation</option>
            <option value="RW" {{ ($action=='edit') ? (($booking->customer_country=='RW') ? 'selected' : '') : '' }}>Rwanda</option>
            <option value="BL" {{ ($action=='edit') ? (($booking->customer_country=='BL') ? 'selected' : '') : '' }}>Saint Barthélemy</option>
            <option value="SH" {{ ($action=='edit') ? (($booking->customer_country=='SH') ? 'selected' : '') : '' }}>Saint Helena, Ascension and Tristan da Cunha</option>
            <option value="KN" {{ ($action=='edit') ? (($booking->customer_country=='KN') ? 'selected' : '') : '' }}>Saint Kitts and Nevis</option>
            <option value="LC" {{ ($action=='edit') ? (($booking->customer_country=='LC') ? 'selected' : '') : '' }}>Saint Lucia</option>
            <option value="MF" {{ ($action=='edit') ? (($booking->customer_country=='MF') ? 'selected' : '') : '' }}>Saint Martin (French part)</option>
            <option value="PM" {{ ($action=='edit') ? (($booking->customer_country=='PM') ? 'selected' : '') : '' }}>Saint Pierre and Miquelon</option>
            <option value="VC" {{ ($action=='edit') ? (($booking->customer_country=='VC') ? 'selected' : '') : '' }}>Saint Vincent and the Grenadines</option>
            <option value="WS" {{ ($action=='edit') ? (($booking->customer_country=='WS') ? 'selected' : '') : '' }}>Samoa</option>
            <option value="SM" {{ ($action=='edit') ? (($booking->customer_country=='SM') ? 'selected' : '') : '' }}>San Marino</option>
            <option value="ST" {{ ($action=='edit') ? (($booking->customer_country=='ST') ? 'selected' : '') : '' }}>Sao Tome and Principe</option>
            <option value="SA" {{ ($action=='edit') ? (($booking->customer_country=='SA') ? 'selected' : '') : '' }}>Saudi Arabia</option>
            <option value="SN" {{ ($action=='edit') ? (($booking->customer_country=='SN') ? 'selected' : '') : '' }}>Senegal</option>
            <option value="RS" {{ ($action=='edit') ? (($booking->customer_country=='RS') ? 'selected' : '') : '' }}>Serbia</option>
            <option value="SC" {{ ($action=='edit') ? (($booking->customer_country=='SC') ? 'selected' : '') : '' }}>Seychelles</option>
            <option value="SL" {{ ($action=='edit') ? (($booking->customer_country=='SL') ? 'selected' : '') : '' }}>Sierra Leone</option>
            <option value="SG" {{ ($action=='edit') ? (($booking->customer_country=='SG') ? 'selected' : '') : '' }}>Singapore</option>
            <option value="SX" {{ ($action=='edit') ? (($booking->customer_country=='SX') ? 'selected' : '') : '' }}>Sint Maarten (Dutch part)</option>
            <option value="SK" {{ ($action=='edit') ? (($booking->customer_country=='SK') ? 'selected' : '') : '' }}>Slovakia</option>
            <option value="SI" {{ ($action=='edit') ? (($booking->customer_country=='SI') ? 'selected' : '') : '' }}>Slovenia</option>
            <option value="SB" {{ ($action=='edit') ? (($booking->customer_country=='SB') ? 'selected' : '') : '' }}>Solomon Islands</option>
            <option value="SO" {{ ($action=='edit') ? (($booking->customer_country=='SO') ? 'selected' : '') : '' }}>Somalia</option>
            <option value="ZA" {{ ($action=='edit') ? (($booking->customer_country=='ZA') ? 'selected' : '') : '' }}>South Africa</option>
            <option value="GS" {{ ($action=='edit') ? (($booking->customer_country=='GS') ? 'selected' : '') : '' }}>South Georgia and the South Sandwich Islands</option>
            <option value="SS" {{ ($action=='edit') ? (($booking->customer_country=='SS') ? 'selected' : '') : '' }}>South Sudan</option>
            <option value="ES" {{ ($action=='edit') ? (($booking->customer_country=='ES') ? 'selected' : '') : '' }}>Spain</option>
            <option value="LK" {{ ($action=='edit') ? (($booking->customer_country=='LK') ? 'selected' : '') : '' }}>Sri Lanka</option>
            <option value="SD" {{ ($action=='edit') ? (($booking->customer_country=='SD') ? 'selected' : '') : '' }}>Sudan</option>
            <option value="SR" {{ ($action=='edit') ? (($booking->customer_country=='SR') ? 'selected' : '') : '' }}>Suriname</option>
            <option value="SJ" {{ ($action=='edit') ? (($booking->customer_country=='SJ') ? 'selected' : '') : '' }}>Svalbard and Jan Mayen</option>
            <option value="SZ" {{ ($action=='edit') ? (($booking->customer_country=='SZ') ? 'selected' : '') : '' }}>Swaziland</option>
            <option value="SE" {{ ($action=='edit') ? (($booking->customer_country=='SE') ? 'selected' : '') : '' }}>Sweden</option>
            <option value="CH" {{ ($action=='edit') ? (($booking->customer_country=='CH') ? 'selected' : '') : '' }}>Switzerland</option>
            <option value="SY" {{ ($action=='edit') ? (($booking->customer_country=='SY') ? 'selected' : '') : '' }}>Syrian Arab Republic</option>
            <option value="TW" {{ ($action=='edit') ? (($booking->customer_country=='TW') ? 'selected' : '') : '' }}>Taiwan, Province of China</option>
            <option value="TJ" {{ ($action=='edit') ? (($booking->customer_country=='TJ') ? 'selected' : '') : '' }}>Tajikistan</option>
            <option value="TZ" {{ ($action=='edit') ? (($booking->customer_country=='TZ') ? 'selected' : '') : '' }}>Tanzania, United Republic of</option>
            <option value="TH" {{ ($action=='edit') ? (($booking->customer_country=='TH') ? 'selected' : '') : '' }}>Thailand</option>
            <option value="TL" {{ ($action=='edit') ? (($booking->customer_country=='TL') ? 'selected' : '') : '' }}>Timor-Leste</option>
            <option value="TG" {{ ($action=='edit') ? (($booking->customer_country=='TG') ? 'selected' : '') : '' }}>Togo</option>
            <option value="TK" {{ ($action=='edit') ? (($booking->customer_country=='TK') ? 'selected' : '') : '' }}>Tokelau</option>
            <option value="TO" {{ ($action=='edit') ? (($booking->customer_country=='TO') ? 'selected' : '') : '' }}>Tonga</option>
            <option value="TT" {{ ($action=='edit') ? (($booking->customer_country=='TT') ? 'selected' : '') : '' }}>Trinidad and Tobago</option>
            <option value="TN" {{ ($action=='edit') ? (($booking->customer_country=='TN') ? 'selected' : '') : '' }}>Tunisia</option>
            <option value="TR" {{ ($action=='edit') ? (($booking->customer_country=='TR') ? 'selected' : '') : '' }}>Turkey</option>
            <option value="TM" {{ ($action=='edit') ? (($booking->customer_country=='TM') ? 'selected' : '') : '' }}>Turkmenistan</option>
            <option value="TC" {{ ($action=='edit') ? (($booking->customer_country=='TC') ? 'selected' : '') : '' }}>Turks and Caicos Islands</option>
            <option value="TV" {{ ($action=='edit') ? (($booking->customer_country=='TV') ? 'selected' : '') : '' }}>Tuvalu</option>
            <option value="UG" {{ ($action=='edit') ? (($booking->customer_country=='UG') ? 'selected' : '') : '' }}>Uganda</option>
            <option value="UA" {{ ($action=='edit') ? (($booking->customer_country=='UA') ? 'selected' : '') : '' }}>Ukraine</option>
            <option value="AE" {{ ($action=='edit') ? (($booking->customer_country=='AE') ? 'selected' : '') : '' }}>United Arab Emirates</option>
            <option value="GB" {{ ($action=='edit') ? (($booking->customer_country=='GB') ? 'selected' : '') : '' }}>United Kingdom</option>
            <option value="US" {{ ($action=='edit') ? (($booking->customer_country=='US') ? 'selected' : '') : '' }}>United States</option>
            <option value="UM" {{ ($action=='edit') ? (($booking->customer_country=='UM') ? 'selected' : '') : '' }}>United States Minor Outlying Islands</option>
            <option value="UY" {{ ($action=='edit') ? (($booking->customer_country=='UY') ? 'selected' : '') : '' }}>Uruguay</option>
            <option value="UZ" {{ ($action=='edit') ? (($booking->customer_country=='UZ') ? 'selected' : '') : '' }}>Uzbekistan</option>
            <option value="VU" {{ ($action=='edit') ? (($booking->customer_country=='VU') ? 'selected' : '') : '' }}>Vanuatu</option>
            <option value="VE" {{ ($action=='edit') ? (($booking->customer_country=='VE') ? 'selected' : '') : '' }}>Venezuela, Bolivarian Republic of</option>
            <option value="VN" {{ ($action=='edit') ? (($booking->customer_country=='VN') ? 'selected' : '') : '' }}>Viet Nam</option>
            <option value="VG" {{ ($action=='edit') ? (($booking->customer_country=='VG') ? 'selected' : '') : '' }}>Virgin Islands, British</option>
            <option value="VI" {{ ($action=='edit') ? (($booking->customer_country=='VI') ? 'selected' : '') : '' }}>Virgin Islands, U.S.</option>
            <option value="WF" {{ ($action=='edit') ? (($booking->customer_country=='WF') ? 'selected' : '') : '' }}>Wallis and Futuna</option>
            <option value="EH" {{ ($action=='edit') ? (($booking->customer_country=='EH') ? 'selected' : '') : '' }}>Western Sahara</option>
            <option value="YE" {{ ($action=='edit') ? (($booking->customer_country=='YE') ? 'selected' : '') : '' }}>Yemen</option>
            <option value="ZM" {{ ($action=='edit') ? (($booking->customer_country=='ZM') ? 'selected' : '') : '' }}>Zambia</option>
            <option value="ZW" {{ ($action=='edit') ? (($booking->customer_country=='ZW') ? 'selected' : '') : '' }}>Zimbabwe</option>
        </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
        <label for="customer_email">Email <span class="text-danger">*</span></label>
        <input type="text" class="form-control {{ $errors->has('customer_email') ? 'is-invalid' : '' }}" id="customer_email" name="customer_email" placeholder="Email" value="{{ ($action=='edit') ? (old('customer_email', $booking->customer_email)) : (old('customer_email')) }}">
        @if($errors->has('customer_email'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('customer_email') }}</strong>
            </div>
        @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="customer_phone">Phone Number</label>
        <input type="number" class="form-control" id="customer_phone" name="customer_phone" placeholder="" value="{{ ($action=='edit') ? (old('customer_phone', $booking->customer_phone)) : (old('customer_phone')) }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
        <label for="comment">Comment</label>
        <textarea class="form-control" id="comment" name="comment" rows="4">{{ ($action=='edit') ? (old('comment', $booking->comment)) : (old('comment')) }}</textarea>
        </div>
    </div>
</div>

<p class="card-description"> Payment Details </p>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
        <label for="price">Price</label>
        <input type="text" class="form-control" id="price" name="price" placeholder="0.00" value="{{ ($action=='edit') ? ($booking->price) : ('0.00') }}" readonly>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="paid">Paid</label>
        <input type="text" class="form-control" id="paid" name="paid" placeholder="0.00" value="{{ ($action=='edit') ? (old('paid', $booking->paid)) : ('0.00') }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="total">Total</label>
        <input type="text" class="form-control" id="total" name="total" placeholder="0.00" value="0.00" readonly>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="invoice_number">Staff</label>
        <input type="text" class="form-control" id="staff" name="staff" value="{{ ($action=='edit') ? (old('staff', $booking->staff)) : '' }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="invoice_number">Invoice No.</label>
        <input type="text" class="form-control" id="invoice_number" name="invoice_number" placeholder="0" value="{{ ($action=='add') ? ((empty($booking->invoice_number)) ? 1 : ($booking->invoice_number + 1)) : (old('invoice_number', $booking->invoice_number)) }}">
        </div>
    </div>
</div>
