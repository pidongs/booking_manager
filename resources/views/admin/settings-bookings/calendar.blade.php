@extends('layouts.master')

@push('head')
<link href='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css' rel='stylesheet' />
<link href='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css' rel='stylesheet' />
<link href='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.css' rel='stylesheet' />
<link href='https://unpkg.com/@fullcalendar/list@4.3.0/main.min.css' rel='stylesheet' />
<link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>

<script src='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js'></script>
<script src='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js'></script>
<script src='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.js'></script>
<script src='https://unpkg.com/@fullcalendar/list@4.3.0/main.min.js'></script>
<script src='https://unpkg.com/@fullcalendar/bootstrap@4.3.0/main.min.js'></script>

<style>
.fc-event {
    padding: 1em;
    /*background-color: white;
    border: 1px #b66dff solid;*/
}
.fc-title {
    color: white;
}

.fc-header-toolbar .fc-left .btn-group button, .fc-header-toolbar .fc-left button, .fc-header-toolbar .fc-right .btn-group button {
    padding: 1em;
}

.tooltip-inner {
    max-width: 100% !important;
    font-size: 18px !important;
    padding: 1em !important;
}
</style>
@endpush

@section('title', 'Calendar | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Calendar </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Bookings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Calendar</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div id='calendar-container'>
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
var calendarEl = document.getElementById('calendar');

var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: [ 'bootstrap', 'dayGrid', 'timeGrid', 'list' ],
    themeSystem: 'bootstrap',
    header: {
        left: 'prev,next today',
        center: 'title',
        // right: 'dayGridMonth,dayGridWeek,dayGridDay,listWeek'
        right: 'dayGridDay,dayGridWeek,dayGridMonth,listWeek'
    },
    firstDay: 1,
    displayEventTime: false,
    defaultView: 'dayGridMonth',
    defaultDate: new Date(),
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: false, // allow "more" link when too many events
    events: "{{ route('bookings.getBookings') }}",
    eventRender: function (info) {
        $(info.el).tooltip({ title: info.event.extendedProps.description });
    },
    eventClick: function(info) {
        var eventObj = info.event;

        window.open(eventObj.url, _parent);
    },
});

calendar.render();
});
</script>
@endpush
