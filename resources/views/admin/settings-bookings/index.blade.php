@extends('layouts.master')

@push('head')
<link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
.dataTables_info, .dataTables_paginate  {
    font-size: small;
}

.dataTables_wrapper .dataTables_filter {
    float: left !important;
}

/* .status, .new {
    float: left;
} */

.status {
    float: left;
}

</style>
@endpush

@section('title', $title . ' | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> {{ $title }} </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Bookings</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <select class="form-control" style="display: hidden;" id="status" name="status">
                    <option value="All">All</option>
                    <option value="Cancelled">Cancelled</option>
                    <option value="New">New</option>
                    <option value="Checkin">Checkin</option>
                    <option value="Checkout">Checkout</option>
                    <option value="Paid">Paid</option>
                    <option value="PaidCC">PaidCC</option>
                </select>

                <table class="table table-hover dt mb-4">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Balance</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(function() {
    var table = $('.dt').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ajax: {
            url: "{{ route($route) }}",
            type: 'GET',
            data: function (d) {
                d.status = $('#status').val();
            }
        },
        language: { search: "", sLengthMenu: "_MENU_" },
        // dom: "f<'status'><'new'>rt<'row'<'ml-4'l><'col-sm-4'i>>",
        // initComplete: function(){
        //     $("div.status").html($("#status"));
        //     $("div.new").html("<a href='{{ route('bookings.create') }}' class='btn btn-gradient-success btn-icon-text ml-3'><i class='mdi mdi-plus'></i> NEW </a>");
        // },
        dom: "f<'status'>rt<'row'<'ml-4'l><'col-sm-4'i>>",
        initComplete: function(){
            $("div.status").html($("#status"));
        },
        columns: [
            { data: 'customer_name', name: 'customer_name', width: '40%' },
            { data: 'balance', name: 'balance', orderable: false, searchable: false, width: '40%' },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: '30%' },
        ],
        "lengthMenu": [[10, 20, 30, 50, 100], [10, 20, 30, 50, 100]],
        "infoCallback": function( settings, start, end, max, total, pre ) {
            return "Showing " + end + " - " + end + " of " + total;
        }
    });

    $('.dataTables_filter input[type="search"]').addClass('form-control mb-4');

    $('div.dataTables_length select').addClass('form-control');

    $('.status').addClass('ml-3');

    $('.dataTables_filter input[type="search"]')
        .attr('placeholder','Search...');

    $('#status').change(function(){
        $('.dt').DataTable().draw(true);
    });
});
</script>
@endpush
