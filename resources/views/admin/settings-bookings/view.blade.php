@extends('layouts.master')

@section('title', 'View Booking | Booking Manager')

@push('head')
<style>
.more::after {
    content: none;
}
</style>
@endpush

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-home-map-marker"></i>
        </span> View Booking </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Bookings</a></li>
            <li class="breadcrumb-item active" aria-current="page">View Booking</li>
        </ol>
        </nav>
    </div>

    @include('layouts._messages')

    <div class="row">
            <div class="col-md-2">
                <form action="{{ route('changeStatus', $booking->id) }}" method="post" class="form-inline">
                    @csrf
                    <div class="form-group">
                        <select class="form-control" id="changeStatus" name="changeStatus">
                            <option value="New" {{ ($booking->status=='New') ? 'selected' : '' }}>New</option>
                            <option value="Approved" {{ ($booking->status=='Approved') ? 'selected' : '' }}>Approved</option>
                            <option value="Modified" {{ ($booking->status=='Modified') ? 'selected' : '' }}>Modified</option>
                            <option value="Checked-in" {{ ($booking->status=='Checked-in') ? 'selected' : '' }}>Checked-in</option>
                            <option value="Checked-out" {{ ($booking->status=='Checked-out') ? 'selected' : '' }}>Checked-out</option>
                            <option value="InvalidCC" {{ ($booking->status=='InvalidCC') ? 'selected' : '' }}>Invalid CC</option>
                            <option value="Debtor" {{ ($booking->status=='Debtor') ? 'selected' : '' }}>Debtor</option>
                            <option value="NoShow" {{ ($booking->status=='NoShow') ? 'selected' : '' }}>No show</option>
                            <option value="Cancelled" {{ ($booking->status=='Cancelled') ? 'selected' : '' }}>Cancelled</option>
                            <option value="Paid" {{ ($booking->status=='Paid') ? 'selected' : '' }}>Paid</option>
                            <option value="PaidCC" {{ ($booking->status=='PaidCC') ? 'selected' : '' }}>Paid CC</option>
                            <option value="Completed" {{ ($booking->status=='Completed') ? 'selected' : '' }}>Completed</option>
                        </select>
                        <button type="submit" class="btn btn-gradient-primary btn-icon-text mx-3">
                        <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>

                        <button type="button" class="btn btn-icon dropdown-toggle more" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <i class="mdi mdi mdi-more" style="font-size: 30px;"></i> </button>
                        <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                            <a href="#" class="dropdown-item p-3"><i class="mdi mdi-printer"></i> Print Invoice</a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item p-3"><i class="mdi mdi-send"></i> Send Invoice</a>
                        </div>
                    </div>
                </form>
            </div>
      </div>

    <div class="row mt-4">
        <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title"><i class="mdi mdi-file-check"></i> Booking Details</h4>
                <div class="media">
                    <div class="media-body">
                    <tr>
                        <td colspan="12">
                            <table>
                            <tr style="height: 50px;">
                                <td>Dates: <strong>{{ $booking->checkin }} - {{ $booking->checkout }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Nights: <strong>{{ $booking->nights }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Guests: <strong>{{ $booking->guests }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Room: <strong>{{ $roomName }}</strong></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title"><i class="mdi mdi-account"></i> Customer Details</h4>
                <div class="media">
                    <div class="media-body">
                    <tr>
                        <td colspan="12">
                            <table>
                            <tr style="height: 50px;">
                                <td>Name: <strong>{{ $booking->customer_name }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Address: <strong>{{ $booking->customer_address }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Country: <strong>{{ $booking->customer_country }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Email: <strong>{{ $booking->customer_email }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Phone: <strong>{{ $booking->customer_phone }}</strong></td>
                            </tr>
                            <tr style="height: 50px;">
                                <td>Comment: <strong>{{ $booking->comment }}</strong></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title"><i class="mdi mdi-coin"></i> Payment Details</h4>
                <div class="media">
                    <div class="media-body">
                    <tr>
                        <td colspan="12">
                            <table>
                                <tr style="height: 50px;">
                                    <td>Price: <strong>{{ $booking->price }}</strong></td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td>Paid: <strong>{{ $booking->paid }}</strong></td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td>Total: <strong>{{ $booking->total }}</strong></td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td>Balance: <strong>{{ $balance }}</strong></td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td>Invoice No.: <strong>{{ $booking->invoice_number }}</strong></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

