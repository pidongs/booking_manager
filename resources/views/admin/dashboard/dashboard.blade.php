@extends('layouts.master')

@section('title', 'Dashboard | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-home"></i>
        </span> Dashboard </h3>
        <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
            </li>
        </ul>
        </nav>
    </div>
    {{-- <div class="row">
        <div class="col-md-4 stretch-card grid-margin">
        <div class="card bg-gradient-danger card-img-holder text-white">
            <div class="card-body">
            <img src="{{ asset('backend_images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
            <h4 class="font-weight-normal mb-3">Weekly Sales <i class="mdi mdi-chart-line mdi-24px float-right"></i>
            </h4>
            <h2 class="mb-5">$ 15,0000</h2>
            <h6 class="card-text">Increased by 60%</h6>
            </div>
        </div>
        </div>
        <div class="col-md-4 stretch-card grid-margin">
        <div class="card bg-gradient-info card-img-holder text-white">
            <div class="card-body">
            <img src="{{ asset('backend_images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
            <h4 class="font-weight-normal mb-3">Weekly Orders <i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
            </h4>
            <h2 class="mb-5">45,6334</h2>
            <h6 class="card-text">Decreased by 10%</h6>
            </div>
        </div>
        </div>
        <div class="col-md-4 stretch-card grid-margin">
        <div class="card bg-gradient-success card-img-holder text-white">
            <div class="card-body">
            <img src="{{ asset('backend_images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
            <h4 class="font-weight-normal mb-3">Visitors Online <i class="mdi mdi-diamond mdi-24px float-right"></i>
            </h4>
            <h2 class="mb-5">95,5741</h2>
            <h6 class="card-text">Increased by 5%</h6>
            </div>
        </div>
        </div>
    </div> --}}

    <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="card-title font-weight-medium mb-0 d-none d-md-block">Statistics</h4>
                            <hr>
                            <div class="d-flex justify-content-between mt-4">
                                <h5 class="font-weight-semibold text-primary">Check-in's</h5>
                                <h5 class="text-primary font-weight-semibold">{{ $todayCheckin }} | {{ $tomorrowCheckin }}</h5>
                            </div>
                            <small class="text-muted">Today | Tomorrow</small>
                            <div class="d-flex justify-content-between mt-4">
                                <h5 class="font-weight-semibold text-danger">Check-out's</h5>
                                <h5 class="text-danger font-weight-semibold">{{ $todayCheckout }} | {{ $tomorrowCheckout }}</h5>
                            </div>
                            <small class="text-muted">Today | Tomorrow</small>
                            <div class="d-flex justify-content-between mt-4">
                                <h5 class="font-weight-semibold text-success">Available</h5>
                                <h5 class="text-success font-weight-semibold">0 | 0</h5>
                            </div>
                            <small class="text-muted">Today | Tomorrow</small>
                            {{-- <div class="d-flex justify-content-between mt-4">
                                <h5 class="font-weight-semibold text-info">Occupied</h5>
                                <h5 class="text-info font-weight-semibold">0</h5>
                            </div>
                            <small class="text-muted">Today</small> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Today Bookings</h4>
                <hr>
                <todaybookings></todaybookings>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
