@extends('layouts.master')

@section('title', 'Invoice Info | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-card-details"></i>
        </span> Invoice Info </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invoice Info</li>
        </ol>
        </nav>
    </div>
    <div class="row">

        <!--FORM-->
        <div class="col-12">
        <div class="card">
            <div class="card-body">
                @include('layouts._messages')
                <form class="form-sample" action="{{ route('invoiceinfo.update', $invoiceInfo->id) }}" method="POST">
                @csrf
                <!--BUTTON SAVE-->
                <div class="pull-right">
                    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
                </div>
                <!--FORM-->
                <p class="card-description"> Invoice info </p>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="logourl">Logo URL</label>
                    <input type="text" class="form-control" id="logourl" name="logourl" placeholder="URL" value="{{ old('logourl', $invoiceInfo->logourl) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="sendinvoice">Send Invoice <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('sendinvoice') ? 'is-invalid' : '' }}" id="sendinvoice" name="sendinvoice" placeholder="0" value="{{ old('sendinvoice', $invoiceInfo->sendinvoice) }}">
                    @if($errors->has('sendinvoice'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('sendinvoice') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="vat">VAT <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('vat') ? 'is-invalid' : '' }}" id="vat" name="vat" placeholder="%" value="{{ old('vat', $invoiceInfo->vat) }}">
                    @if($errors->has('vat'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('vat') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="invoicetext">Invoice Text</label>
                    <textarea class="form-control" id="invoicetext" name="invoicetext" rows="4">{{ old('invoicetext', $invoiceInfo->invoicetext) }}</textarea>
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="invoiceprefix">Invoice Prefix</label>
                    <input type="text" class="form-control" id="invoiceprefix" name="invoiceprefix" value="{{ old('invoiceprefix', $invoiceInfo->invoiceprefix) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="sendapproval">Send Approval <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('sendapproval') ? 'is-invalid' : '' }}" id="sendapproval" name="sendapproval" placeholder="0" value="{{ old('sendapproval', $invoiceInfo->sendapproval) }}">
                    @if($errors->has('sendapproval'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('sendapproval') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="currency">Currency <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('currency') ? 'is-invalid' : '' }}" id="currency" name="currency" value="{{ old('currency', $invoiceInfo->currency) }}">
                    @if($errors->has('currency'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('currency') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="approvaltext">Approval Text</label>
                    <textarea class="form-control" id="approvaltext" name="approvaltext" rows="4">{{ old('approvaltext', $invoiceInfo->approvaltext) }}</textarea>
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="companydetails">Company Details</label>
                    <textarea class="form-control" id="companydetails" name="companydetails" rows="4">{{ old('companydetails', $invoiceInfo->companydetails) }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="4">{{ old('description', $invoiceInfo->description) }}</textarea>
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="invoicefooter">Invoice Footer</label>
                    <input type="text" class="form-control" id="invoicefooter" name="invoicefooter" value="{{ old('invoicefooter', $invoiceInfo->invoicefooter) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="signaturetext">Signature Text</label>
                    <textarea class="form-control" id="signaturetext" name="signaturetext" rows="4">{{ old('signaturetext', $invoiceInfo->signaturetext) }}</textarea>
                    </div>
                </div>
                </div>

            </form>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
