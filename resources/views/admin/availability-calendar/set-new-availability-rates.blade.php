@extends('layouts.master')

@push('head')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush

@section('title', 'Set Availability and Rates | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Set Availability and Rates </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Availability Calendar</a></li>
            <li class="breadcrumb-item active" aria-current="page">Set Availability and Rates</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <form class="set-availability-rates-form" action="{{ route('updateAvailabilityRates') }}" method="POST">
                    @csrf
                      <div class="pull-right">
                          <button type="submit" class="btn btn-gradient-warning btn-icon-text">
                            <i class="mdi mdi-file-check btn-icon-prepend"></i> SET </button>
                      </div>
                      <!--FORM-->
                      <p class="card-description"> Set availability and rates </p>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <p class="card-description"> Room: </p>
                            <div class="form-check form-check-info">
                                <label class="form-check-label"><input type="checkbox" class="form-check-input" id="selectAllRooms"> All </label>
                            </div>

                            @foreach($rooms as $room)
                            <div class="form-check form-check-info">
                                <label class="form-check-label"><input type="checkbox" class="form-check-input" id="room_cb" name="room_cb[]" value={{ $room->id }}> {{ $room->name }} </label>
                            </div>
                            @endforeach

                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <p class="card-description"> Weekday: </p>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" id="selectAllDays"> All </label>
                            </div>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Monday"> Monday </label>
                            </div>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Tuesday"> Tuesday </label>
                            </div>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Wednesday"> Wednesday </label>
                            </div>
                                <div class="form-check form-check-info">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Thursday"> Thursday </label>
                              </div>
                              <div class="form-check form-check-info">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Friday"> Friday </label>
                              </div>
                              <div class="form-check form-check-info">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Saturday"> Saturday </label>
                              </div>
                              <div class="form-check form-check-info">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="day_cb" name="day_cb[]" value="Sunday"> Sunday </label>
                               </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="date">Date Range</label>
                            <input type="hidden" id="start" name="start_date">
                            <input type="hidden" id="end" name="end_date">
                            <input type="text" class="form-control" id="daterange" readonly>
                          </br>
                          <label for="option">Option</label>
                          <select class="form-control" id="option" name="option">
                            <option value='setQuantity'>Set Quantity</option>
                            <option value='setPrice'>Set Price</option>
                          </select>
                         </br>
                            <label for="value">Value</label>
                            <input type="number" class="form-control {{ $errors->has('value') ? 'is-invalid' : '' }}" id="value" name="value" value={{ old('value', 0) }}>
                            @if($errors->has('value'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </div>
                            @endif
                            </div>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(function() {
    $('#start').val(moment().format('MM/DD/YYYY'));
    $('#end').val(moment().add(1, 'day').format('MM/DD/YYYY'));

    $('#daterange').daterangepicker({
        "autoApply": true,
        "startDate": moment().format('MM/DD/YYYY'),
        "endDate": moment().add(1, 'day').format('MM/DD/YYYY'),
        "minDate": moment().format('MM/DD/YYYY'),
    }, function(start, end) {
        $('#start').val(start.format('MM/DD/YYYY'));
        $('#end').val(end.format('MM/DD/YYYY'));
    });

    $('#selectAllRooms').click(function(){
        if($(this).is(':checked') ){
            $("input[id=room_cb]").each(function(){
                $(this).prop('checked', true);
            });
        }
        else{
            $("input[id=room_cb]").each(function(){
                $(this).prop('checked', false);
            });
        }
    });

    $('#selectAllRooms').click(function(){
        $("input[id=room_cb]").each(function(){
            $(this).prop('checked', $(this).prop('checked'));
        });
    });

    $('input[id=room_cb]').click(function(){
        if(!$(this).prop("checked")){
            $('#selectAllRooms').prop('checked', false);
        }
    });

    $('#selectAllDays').click(function(){
        $("input[id=day_cb]").prop('checked', $(this).prop('checked'));
    });

    $('input[id=day_cb]').click(function(){
        if(!$(this).prop("checked")){
            $('#selectAllDays').prop('checked', false);
        }
    });
});
</script>
@endpush
