@extends('layouts.master')

@push('head')
<link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
table.dataTable tbody td {
    padding: 20px;
}

.dataTables_info, .dataTables_paginate  {
    font-size: small;
}

.dataTables_wrapper .dataTables_filter {
    float: left !important;
}

table.dataTable thead th {
    background-color: #fff !important;
    padding: 20px 28px;
}

.daterange, .set {
    float: left;
}

.set{
    margin-left: 25px;
}

</style>
@endpush

@section('title', 'Calendar | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Calendar </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Availability Calendar</a></li>
            <li class="breadcrumb-item active" aria-current="page">Calendar</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <form id="getDates" name="getDates" action="{{ route('availabilityGetDates') }}" method="POST">
                    @csrf
                    <input type="hidden" id="start" name="start_date">
                    <input type="hidden" id="end" name="end_date">
                    <input type="text" class="form-control" style="display: hidden;" id="daterange" name="daterange">
                </form>

                <table class="table table-hover dt mb-6">

                </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
<script>
$(function() {
    function getTableColumns (){
        var cols;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('getTableColumns') }}",
            method: 'get',
            async: false,
            data: {
                start_date: $('#start').val(),
                end_date: $('#end').val()
            },
            success: function(result){
                cols = result;
            }
        });

        return cols;
    }

    function checkBooking(room_id){
        var cols;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "",
            method: 'get',
            async: false,
            data: {
                room_id: room_id
            },
            success: function(result){
                cols = result;
            }
        });

        return cols;
    }

    var s = "{{ $from }}";
    var e = "{{ $to }}";

    $('#start').val(moment(s).format('MM/DD/YYYY'));
    $('#end').val(moment(e).format('MM/DD/YYYY'));

    $('#daterange').daterangepicker({
        "autoApply": true,
        "startDate": moment(s).format('MM/DD/YYYY'),
        "endDate": moment(e).format('MM/DD/YYYY'),
        "minDate": moment().format('MM/DD/YYYY'),
    }, function(start, end) {
        $('#start').val(start.format('MM/DD/YYYY'));
        $('#end').val(end.format('MM/DD/YYYY'));
        $('#getDates').submit();
    });

    var table = $(".dt").DataTable({
        processing: true,
        autoWidth: false,
        scrollX: true,
        ajax: {
            url: "{{ route('dataAvailability') }}",
            type: 'post',
            data: function (d) {
                d.start_date = $('#start').val();
                d.end_date = $('#end').val();
            }
        },
        language: { search: "", sLengthMenu: "_MENU_" },
        dom: "f<'daterange'>rt<'row'<'ml-4 mt-4'l><'col-sm-4 mt-4'i>>",
        initComplete: function(){
            $("div.daterange").html($("#daterange"));
        },
        columns: getTableColumns(),
        "lengthMenu": [[10, 20, 30, 50, 100], [10, 20, 30, 50, 100]],
        "infoCallback": function( settings, start, end, max, total, pre ) {
            return "Showing " + end + " - " + end + " of " + total;
        },
        'columnDefs': [
            {
                "targets": "_all",
                "className": "text-center",
            },
            {
                "render": function ( data, type, row, meta ){
                    return data;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row, meta ){
                    var res = data.split("|");
                    var title = $('.dt').DataTable().columns( meta.col ).header();
                    var columnName = $(title).html();

                    var quantityLink = '<a href="{{ route('setAvailabilityRates', [':room', ':date', ':option', ':value']) }}">'+res[1]+'</a>';
                    quantityLink = quantityLink.replace(':room',res[0]);
                    quantityLink = quantityLink.replace(':date',columnName);
                    quantityLink = quantityLink.replace(':option','setQuantity');
                    quantityLink = quantityLink.replace(':value',res[1]);

                    var priceLink = '<a href="{{ route('setAvailabilityRates', [':room', ':date', ':option', ':value']) }}">'+res[2]+'</a>';
                    priceLink = priceLink.replace(':room',res[0]);
                    priceLink = priceLink.replace(':date',columnName);
                    priceLink = priceLink.replace(':option','setPrice');
                    priceLink = priceLink.replace(':value',res[2]);

                    return quantityLink+'<br><br>'+priceLink;
                },
                "targets": "_all"
            },
        ],
        "createdRow": function(row, data, index){
            //console.log(row);
            //$(row).find('td:gt(0) a').attr('style', 'color: #fff !important');
            //$(row).find('td:gt(0)').attr('style', 'background-color: #47894b !important');
        },
        fixedColumns: {
            leftColumns: 1
        }
    });

    $('.dataTables_filter input[type="search"]').addClass('form-control mb-4');

    $('div.dataTables_length select').addClass('form-control');

    $('#daterange').addClass('ml-3');

    $('.dataTables_filter input[type="search"]')
        .attr('placeholder','Search...');
});
</script>
@endpush

