@extends('layouts.master')

@section('title', 'Payment Info | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-credit-card-multiple"></i>
        </span> Payment Info </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Payment Info</li>
        </ol>
        </nav>
    </div>
    <div class="row">

        <!--FORM-->
        <div class="col-12">
        <div class="card">
            <div class="card-body">
                @include('layouts._messages')
                <form class="form-sample" action="{{ route('paymentinfo.update', $paymentInfo->id) }}" method="POST">
                @csrf
                <!--BUTTON SAVE-->
                <div class="pull-right">
                    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
                </div>
                <!--FORM-->
                <p class="card-description"> Payment info </p>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="requireprepay">Require Prepay (1 active) <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('requireprepay') ? 'is-invalid' : '' }}" id="requireprepay" name="requireprepay" placeholder="0" value="{{ old('requireprepay', $paymentInfo->requireprepay) }}">
                    @if($errors->has('requireprepay'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('requireprepay') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="prepaymore7d">Prepay percent more than 7 days</label>
                    <input type="number" class="form-control" id="prepaymore7d" name="prepaymore7d" placeholder="0" value="{{ old('prepaymore7d', $paymentInfo->prepaymore7d) }}">
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="prepayless7d">Prepay percent less than 7 days</label>
                    <input type="number" class="form-control" id="prepayless7d" name="prepayless7d" placeholder="0" value="{{ old('prepayless7d', $paymentInfo->prepayless7d) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="prepaytext">Prepay Text, shortcode [prepay]</label>
                    <textarea class="form-control" id="prepaytext" name="prepaytext" rows="4">{{ old('prepaytext', $paymentInfo->prepay) }}</textarea>
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="prepay_success">Prepay Success Text, shortcode [prepay_success]</label>
                    <textarea class="form-control" id="prepay_success" name="prepay_success" rows="4">{{ old('prepay_success', $paymentInfo->prepay_success) }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="prepay_fail">Prepay Fail Text, shortcode [prepay_fail]</label>
                    <textarea class="form-control" id="prepay_fail" name="prepay_fail" rows="4">{{ old('prepay_fail', $paymentInfo->prepay_fail) }}</textarea>
                    </div>
                </div>
                </div>

            </form>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
