@extends('layouts.master')

@push('head')
<link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
/* Toggle Button */
.cm-toggle {
	-webkit-appearance: none;
	-webkit-tap-highlight-color: transparent;
	position: relative;
	border: 0;
	outline: 0;
	cursor: pointer;
	margin: 10px;
}


/* To create surface of toggle button */
.cm-toggle:after {
	content: '';
	width: 40px;
	height: 18px;
	display: inline-block;
	background: rgba(196, 195, 195, 0.55);
	border-radius: 18px;
	clear: both;
}


/* Contents before checkbox to create toggle handle */
.cm-toggle:before {
	content: '';
	width: 21px;
	height: 21px;
	display: block;
	position: absolute;
	left: 0;
	top: -2px;
	border-radius: 50%;
	background: rgb(255, 255, 255);
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
}


/* Shift the handle to left on check event */
.cm-toggle:checked:before {
	left: 22px;
	box-shadow: -1px 1px 3px rgba(0, 0, 0, 0.6);
}
/* Background color when toggle button will be active */
.cm-toggle:checked:after {
	background: #16a085;
}

/* Transition for smoothness */
.cm-toggle,
.cm-toggle:before,
.cm-toggle:after,
.cm-toggle:checked:before,
.cm-toggle:checked:after {
	transition: ease .3s;
	-webkit-transition: ease .3s;
	-moz-transition: ease .3s;
	-o-transition: ease .3s;
}

.dataTables_info, .dataTables_paginate  {
    font-size: small;
}

.toolbar {
    float: right;
}

td {
    white-space: normal !important;
    word-break: break-word;
}

</style>
@endpush

@section('title', 'Rooms | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-hotel"></i>
        </span> Rooms </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Rooms</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <table class="table table-responsive mb-4 dt">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th >Description</th>
                        <th>Safebox</th>
                        <th>Bookable</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(function() {
    var table = $('.dt').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        ajax: "{{ route('rooms.index') }}",
        language: { search: "", sLengthMenu: "_MENU_" },
        dom: "<'toolbar'>frt<'row'<'ml-4'l><'col-sm-4'i>>",
        initComplete: function(){
            $("div.toolbar").html("<a href='{{ route('rooms.create') }}' class='btn btn-gradient-info btn-icon-text ml-3'><i class='mdi mdi-plus'></i> CREATE </a>");
        },
        columns: [
                { data: 'id', name: 'id'},
                { data: 'name', name: 'name' },
                { data: 'type', name: 'type', orderable: false },
                { data: 'description', name: 'description', orderable: false },
                { data: 'safebox', name: 'safebox', orderable: false },
                { data: 'bookable', name: 'bookable', orderable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        "lengthMenu": [[10, 20, 30, 50, 100], [10, 20, 30, 50, 100]],
        "infoCallback": function( settings, start, end, max, total, pre ) {
            return "Showing " + end + " - " + end + " of " + total;
        },
        order: [[0, 'desc']],
        'columnDefs': [
        {
            "targets": [0,5],
            "className": "text-center"
        },
        {
            "targets": [1,2,3,4,5,6],
            "width": "20%",
        },
        {
            "targets": 0,
            "width": "8%",
        }],
    });

    $('div.dataTables_filter input[type="search"]').addClass('form-control mb-4');

    $('div.dataTables_length select').addClass('form-control');

    $('.dataTables_filter input[type="search"]')
        .attr('placeholder','Search...');

    $('.dt tbody').on('click', '.deleteRoom', function (e) {
        e.preventDefault();
        swal({
            text: "Are you sure you want to delete this record?",
            icon: "warning",
            buttons: true,
            buttons: {
                cancel: {
                    text: "Close",
                    value: null,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "btn btn-primary",
                    closeModal: true
                }
            }
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('form').submit();
            }
        })
        .catch((error) => {
            swal('Error', 'Room has not been deleted.', 'error');
        })
    });

    $('.dt tbody').on('change', '.book', function (e) {
        var val = $(this).parent().find("input[name='bookable']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

});

</script>
@endpush
