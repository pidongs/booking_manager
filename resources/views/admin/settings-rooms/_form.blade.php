@csrf
<input type="hidden" value="{{ auth()->user()->id }}" id="user_id" name="user_id">
<!--BUTTON SAVE-->
<div class="pull-right">
    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
</div>

<p class="card-description"> Type, room or property info </p>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="title'">Title</label>
            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Name" value="{{ old('name', $room->name) }}" maxlength="100">
            @if($errors->has('name'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="type">Type</label>
            <select class="form-control" id="type" name="type">
                <option value="Room">Room</option>
                <option value="Property">Property</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="ordernumber">Order Number</label>
            <input type="number" class="form-control {{ $errors->has('ordernumber') ? 'is-invalid' : '' }}" id="ordernumber" name="ordernumber" value="{{ old('ordernumber', $room->ordernumber) }}">
            @if($errors->has('ordernumber'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('ordernumber') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="guests">Guests</label>
            <input type="number" class="form-control {{ $errors->has('guests') ? 'is-invalid' : '' }}" id="guests" name="guests" value="{{ old('guests', $room->guests) }}">
            @if($errors->has('guests'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('guests') }}</strong>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <label for="quantity">Quantity</label>
            <input type="text" class="form-control" id="quantity" name="quantity" value="{{ old('quantity', $room->quantity) }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
        <label for="price">Room Price</label>
            <input type="text" class="form-control" id="price" name="price" value="{{ old('price', $room->price) }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <label for="price_per_guest">Price per Guest</label>
            <input type="text" class="form-control" id="price_per_guest" name="price_per_guest" value="{{ old('price_per_guest', $room->price_per_guest) }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
        <label for="safebox">Safebox</label>
        <textarea class="form-control" id="safebox" name="safebox" rows="4" maxlength="255">{{ old('safebox', $room->safebox) }}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" id="description" name="description" rows="4" maxlength="255">{{ old('description', $room->description) }}</textarea>
            @if($errors->has('description'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
        <label for="photourl">Photo URL </br><small style="color:cornflowerblue">(ex. https://yourdomain.com/yourphoto)</small></label>
        <input type="text" class="form-control" id="photourl" name="photourl" value="{{ old('photourl', $room->photourl) }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <label for="pageurl">Page URL </br><small style="color:cornflowerblue">(ex. https://yourdomain.com/yourpage)</small></label>
        <input type="text" class="form-control" id="pageurl" name="pageurl" value="{{ old('pageurl', $room->pageurl) }}">
        </div>
    </div>
</div>
