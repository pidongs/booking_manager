@extends('layouts.master')

@section('title', 'Rooms | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-hotel"></i>
        </span> Create type, room or property </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item"><a href="#">Rooms</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
        </ol>
        </nav>
    </div>
    <div class="row">

        <!--FORM-->
        <div class="col-12">
        <div class="card">
            <div class="card-body">
            @include('layouts._messages')
            <form class="form-sample" action="{{ route('rooms.store') }}" method="POST">
                @include('admin.settings-rooms._form', ['buttonText' => 'Save'])
            </form>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
