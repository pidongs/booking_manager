@extends('layouts.master')

@section('title', 'Property Info | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-home"></i>
        </span> Property Info </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Property Info</li>
        </ol>
        </nav>
    </div>
    <div class="row">

    <!--FORM-->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            @include('layouts._messages')
            <form class="form-sample" action="{{ route('propertyinfo.update', $propertyInfo->id) }}" method="POST">
                @csrf
                <!--BUTTON SAVE-->
                <div class="pull-right">
                    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
                </div>

                <p class="card-description"> Property Info </p>
                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="title">Title <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title" name="title" placeholder="Title" value="{{ old('title', $propertyInfo->title) }}">
                    @if($errors->has('title'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('title') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="phone">Phone <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" name="phone" value="{{ old('phone', $propertyInfo->phone) }}">
                    @if($errors->has('phone'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="requirecc">Require CC</label>
                    <input type="number" class="form-control" id="requirecc" name="requirecc" placeholder="0" value="{{ old('requirecc', $propertyInfo->requirecc) }}">
                    </div>
                </div>
                </div>

            <!--LABEL-->
            <!-- <p class="card-description"> Address </p> -->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="type">Type <span class="text-danger">*</span></label>
                    <select class="form-control" name="type" id="type">
                        <option value="Apartments" {{ ($propertyInfo->type=='Apartments') ? 'selected' : '' }}>Apartments</option>
                        <option value="Hostel" {{ ($propertyInfo->type=='Hostel') ? 'selected' : '' }}>Hostel</option>
                        <option value="Hotel" {{ ($propertyInfo->type=='Hotel') ? 'selected' : '' }}>Hotel</option>
                        <option value="GuestHouse" {{ ($propertyInfo->type=='GuestHouse') ? 'selected' : '' }}>Guest House</option>
                        <option value="Dormitory" {{ ($propertyInfo->type=='Dormitory') ? 'selected' : '' }}>Dormitory</option>
                        <option value="Rental" {{ ($propertyInfo->type=='Rental') ? 'selected' : '' }}>Rental</option>
                        <option value="Resort" {{ ($propertyInfo->type=='Resort') ? 'selected' : '' }}>Resort</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="email">Email <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" name="email" placeholder="Email" value="{{ old('email', $propertyInfo->email) }}">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="coordinates">Coordinates</label>
                    <input type="text" class="form-control" id="coordinates" name="coordinates" placeholder="Coordinates" value="{{ old('coordinates', $propertyInfo->coordinates) }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="logourl">Logo URL</label>
                    <input type="text" class="form-control" id="logourl" name="logourl" placeholder="URL" value="{{ old('logourl', $propertyInfo->logourl) }}">
                    </div>
                </div>
            </div>

                <div class="row">
                {{-- <div class="col-md-4">
                    <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text" class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" id="country" name="country" placeholder="Country" value="{{ old('country', $propertyInfo->country) }}">
                    @if($errors->has('country'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('country') }}</strong>
                        </div>
                    @endif
                    </div>
                </div> --}}
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="country">Country <span class="text-danger">*</span></label>
                    <select class="form-control" id="country" name="country">
                        <option value="AF" {{ ($propertyInfo->country=='AF') ? 'selected' : '' }}>Afghanistan</option>
                        <option value="AX" {{ ($propertyInfo->country=='AX') ? 'selected' : '' }}>Åland Islands</option>
                        <option value="AL" {{ ($propertyInfo->country=='AL') ? 'selected' : '' }}>Albania</option>
                        <option value="DZ" {{ ($propertyInfo->country=='DZ') ? 'selected' : '' }}>Algeria</option>
                        <option value="AS" {{ ($propertyInfo->country=='AS') ? 'selected' : '' }}>American Samoa</option>
                        <option value="AD" {{ ($propertyInfo->country=='AD') ? 'selected' : '' }}>Andorra</option>
                        <option value="AO" {{ ($propertyInfo->country=='AO') ? 'selected' : '' }}>Angola</option>
                        <option value="AI" {{ ($propertyInfo->country=='AI') ? 'selected' : '' }}>Anguilla</option>
                        <option value="AQ" {{ ($propertyInfo->country=='AQ') ? 'selected' : '' }}>Antarctica</option>
                        <option value="AG" {{ ($propertyInfo->country=='AG') ? 'selected' : '' }}>Antigua and Barbuda</option>
                        <option value="AR" {{ ($propertyInfo->country=='AR') ? 'selected' : '' }}>Argentina</option>
                        <option value="AM" {{ ($propertyInfo->country=='AM') ? 'selected' : '' }}>Armenia</option>
                        <option value="AW" {{ ($propertyInfo->country=='AW') ? 'selected' : '' }}>Aruba</option>
                        <option value="AU" {{ ($propertyInfo->country=='AU') ? 'selected' : '' }}>Australia</option>
                        <option value="AT" {{ ($propertyInfo->country=='AT') ? 'selected' : '' }}>Austria</option>
                        <option value="AZ" {{ ($propertyInfo->country=='AZ') ? 'selected' : '' }}>Azerbaijan</option>
                        <option value="BS" {{ ($propertyInfo->country=='BS') ? 'selected' : '' }}>Bahamas</option>
                        <option value="BH" {{ ($propertyInfo->country=='BH') ? 'selected' : '' }}>Bahrain</option>
                        <option value="BD" {{ ($propertyInfo->country=='BD') ? 'selected' : '' }}>Bangladesh</option>
                        <option value="BB" {{ ($propertyInfo->country=='BB') ? 'selected' : '' }}>Barbados</option>
                        <option value="BY" {{ ($propertyInfo->country=='BY') ? 'selected' : '' }}>Belarus</option>
                        <option value="BE" {{ ($propertyInfo->country=='BE') ? 'selected' : '' }}>Belgium</option>
                        <option value="BZ" {{ ($propertyInfo->country=='BZ') ? 'selected' : '' }}>Belize</option>
                        <option value="BJ" {{ ($propertyInfo->country=='BJ') ? 'selected' : '' }}>Benin</option>
                        <option value="BM" {{ ($propertyInfo->country=='BM') ? 'selected' : '' }}>Bermuda</option>
                        <option value="BT" {{ ($propertyInfo->country=='BT') ? 'selected' : '' }}>Bhutan</option>
                        <option value="BO" {{ ($propertyInfo->country=='BO') ? 'selected' : '' }}>Bolivia, Plurinational State of</option>
                        <option value="BQ" {{ ($propertyInfo->country=='BQ') ? 'selected' : '' }}>Bonaire, Sint Eustatius and Saba</option>
                        <option value="BA" {{ ($propertyInfo->country=='BA') ? 'selected' : '' }}>Bosnia and Herzegovina</option>
                        <option value="BW" {{ ($propertyInfo->country=='BW') ? 'selected' : '' }}>Botswana</option>
                        <option value="BV" {{ ($propertyInfo->country=='BV') ? 'selected' : '' }}>Bouvet Island</option>
                        <option value="BR" {{ ($propertyInfo->country=='BR') ? 'selected' : '' }}>Brazil</option>
                        <option value="IO" {{ ($propertyInfo->country=='IO') ? 'selected' : '' }}>British Indian Ocean Territory</option>
                        <option value="BN" {{ ($propertyInfo->country=='BN') ? 'selected' : '' }}>Brunei Darussalam</option>
                        <option value="BG" {{ ($propertyInfo->country=='BG') ? 'selected' : '' }}>Bulgaria</option>
                        <option value="BF" {{ ($propertyInfo->country=='BF') ? 'selected' : '' }}>Burkina Faso</option>
                        <option value="BI" {{ ($propertyInfo->country=='BI') ? 'selected' : '' }}>Burundi</option>
                        <option value="KH" {{ ($propertyInfo->country=='KH') ? 'selected' : '' }}>Cambodia</option>
                        <option value="CM" {{ ($propertyInfo->country=='CM') ? 'selected' : '' }}>Cameroon</option>
                        <option value="CA" {{ ($propertyInfo->country=='CA') ? 'selected' : '' }}>Canada</option>
                        <option value="CV" {{ ($propertyInfo->country=='CV') ? 'selected' : '' }}>Cape Verde</option>
                        <option value="KY" {{ ($propertyInfo->country=='KY') ? 'selected' : '' }}>Cayman Islands</option>
                        <option value="CF" {{ ($propertyInfo->country=='CF') ? 'selected' : '' }}>Central African Republic</option>
                        <option value="TD" {{ ($propertyInfo->country=='TD') ? 'selected' : '' }}>Chad</option>
                        <option value="CL" {{ ($propertyInfo->country=='CL') ? 'selected' : '' }}>Chile</option>
                        <option value="CN" {{ ($propertyInfo->country=='CN') ? 'selected' : '' }}>China</option>
                        <option value="CX" {{ ($propertyInfo->country=='CX') ? 'selected' : '' }}>Christmas Island</option>
                        <option value="CC" {{ ($propertyInfo->country=='CC') ? 'selected' : '' }}>Cocos (Keeling) Islands</option>
                        <option value="CO" {{ ($propertyInfo->country=='CO') ? 'selected' : '' }}>Colombia</option>
                        <option value="KM" {{ ($propertyInfo->country=='KM') ? 'selected' : '' }}>Comoros</option>
                        <option value="CG" {{ ($propertyInfo->country=='CG') ? 'selected' : '' }}>Congo</option>
                        <option value="CD" {{ ($propertyInfo->country=='CD') ? 'selected' : '' }}>Congo, the Democratic Republic of the</option>
                        <option value="CK" {{ ($propertyInfo->country=='CK') ? 'selected' : '' }}>Cook Islands</option>
                        <option value="CR" {{ ($propertyInfo->country=='CR') ? 'selected' : '' }}>Costa Rica</option>
                        <option value="CI" {{ ($propertyInfo->country=='CI') ? 'selected' : '' }}>Côte d'Ivoire</option>
                        <option value="HR" {{ ($propertyInfo->country=='HR') ? 'selected' : '' }}>Croatia</option>
                        <option value="CU" {{ ($propertyInfo->country=='CU') ? 'selected' : '' }}>Cuba</option>
                        <option value="CW" {{ ($propertyInfo->country=='CW') ? 'selected' : '' }}>Curaçao</option>
                        <option value="CY" {{ ($propertyInfo->country=='CY') ? 'selected' : '' }}>Cyprus</option>
                        <option value="CZ" {{ ($propertyInfo->country=='CZ') ? 'selected' : '' }}>Czech Republic</option>
                        <option value="DK" {{ ($propertyInfo->country=='DK') ? 'selected' : '' }}>Denmark</option>
                        <option value="DJ" {{ ($propertyInfo->country=='DJ') ? 'selected' : '' }}>Djibouti</option>
                        <option value="DM" {{ ($propertyInfo->country=='DM') ? 'selected' : '' }}>Dominica</option>
                        <option value="DO" {{ ($propertyInfo->country=='DO') ? 'selected' : '' }}>Dominican Republic</option>
                        <option value="EC" {{ ($propertyInfo->country=='EC') ? 'selected' : '' }}>Ecuador</option>
                        <option value="EG" {{ ($propertyInfo->country=='EG') ? 'selected' : '' }}>Egypt</option>
                        <option value="SV" {{ ($propertyInfo->country=='SV') ? 'selected' : '' }}>El Salvador</option>
                        <option value="GQ" {{ ($propertyInfo->country=='GQ') ? 'selected' : '' }}>Equatorial Guinea</option>
                        <option value="ER" {{ ($propertyInfo->country=='ER') ? 'selected' : '' }}>Eritrea</option>
                        <option value="EE" {{ ($propertyInfo->country=='EE') ? 'selected' : '' }}>Estonia</option>
                        <option value="ET" {{ ($propertyInfo->country=='ET') ? 'selected' : '' }}>Ethiopia</option>
                        <option value="FK" {{ ($propertyInfo->country=='FK') ? 'selected' : '' }}>Falkland Islands (Malvinas)</option>
                        <option value="FO" {{ ($propertyInfo->country=='FO') ? 'selected' : '' }}>Faroe Islands</option>
                        <option value="FJ" {{ ($propertyInfo->country=='FJ') ? 'selected' : '' }}>Fiji</option>
                        <option value="FI" {{ ($propertyInfo->country=='FI') ? 'selected' : '' }}>Finland</option>
                        <option value="FR" {{ ($propertyInfo->country=='FR') ? 'selected' : '' }}>France</option>
                        <option value="GF" {{ ($propertyInfo->country=='GF') ? 'selected' : '' }}>French Guiana</option>
                        <option value="PF" {{ ($propertyInfo->country=='PF') ? 'selected' : '' }}>French Polynesia</option>
                        <option value="TF" {{ ($propertyInfo->country=='TF') ? 'selected' : '' }}>French Southern Territories</option>
                        <option value="GA" {{ ($propertyInfo->country=='GA') ? 'selected' : '' }}>Gabon</option>
                        <option value="GM" {{ ($propertyInfo->country=='GM') ? 'selected' : '' }}>Gambia</option>
                        <option value="GE" {{ ($propertyInfo->country=='GE') ? 'selected' : '' }}>Georgia</option>
                        <option value="DE" {{ ($propertyInfo->country=='DE') ? 'selected' : '' }}>Germany</option>
                        <option value="GH" {{ ($propertyInfo->country=='GH') ? 'selected' : '' }}>Ghana</option>
                        <option value="GI" {{ ($propertyInfo->country=='GI') ? 'selected' : '' }}>Gibraltar</option>
                        <option value="GR" {{ ($propertyInfo->country=='GR') ? 'selected' : '' }}>Greece</option>
                        <option value="GL" {{ ($propertyInfo->country=='GL') ? 'selected' : '' }}>Greenland</option>
                        <option value="GD" {{ ($propertyInfo->country=='GD') ? 'selected' : '' }}>Grenada</option>
                        <option value="GP" {{ ($propertyInfo->country=='GP') ? 'selected' : '' }}>Guadeloupe</option>
                        <option value="GU" {{ ($propertyInfo->country=='GU') ? 'selected' : '' }}>Guam</option>
                        <option value="GT" {{ ($propertyInfo->country=='GT') ? 'selected' : '' }}>Guatemala</option>
                        <option value="GG" {{ ($propertyInfo->country=='GG') ? 'selected' : '' }}>Guernsey</option>
                        <option value="GN" {{ ($propertyInfo->country=='GN') ? 'selected' : '' }}>Guinea</option>
                        <option value="GW" {{ ($propertyInfo->country=='GW') ? 'selected' : '' }}>Guinea-Bissau</option>
                        <option value="GY" {{ ($propertyInfo->country=='GY') ? 'selected' : '' }}>Guyana</option>
                        <option value="HT" {{ ($propertyInfo->country=='HT') ? 'selected' : '' }}>Haiti</option>
                        <option value="HM" {{ ($propertyInfo->country=='HM') ? 'selected' : '' }}>Heard Island and McDonald Islands</option>
                        <option value="VA" {{ ($propertyInfo->country=='VA') ? 'selected' : '' }}>Holy See (Vatican City State)</option>
                        <option value="HN" {{ ($propertyInfo->country=='HN') ? 'selected' : '' }}>Honduras</option>
                        <option value="HK" {{ ($propertyInfo->country=='HK') ? 'selected' : '' }}>Hong Kong</option>
                        <option value="HU" {{ ($propertyInfo->country=='HU') ? 'selected' : '' }}>Hungary</option>
                        <option value="IS" {{ ($propertyInfo->country=='IS') ? 'selected' : '' }}>Iceland</option>
                        <option value="IN" {{ ($propertyInfo->country=='IN') ? 'selected' : '' }}>India</option>
                        <option value="ID" {{ ($propertyInfo->country=='ID') ? 'selected' : '' }}>Indonesia</option>
                        <option value="IR" {{ ($propertyInfo->country=='IR') ? 'selected' : '' }}>Iran, Islamic Republic of</option>
                        <option value="IQ" {{ ($propertyInfo->country=='IQ') ? 'selected' : '' }}>Iraq</option>
                        <option value="IE" {{ ($propertyInfo->country=='IE') ? 'selected' : '' }}>Ireland</option>
                        <option value="IM" {{ ($propertyInfo->country=='IM') ? 'selected' : '' }}>Isle of Man</option>
                        <option value="IL" {{ ($propertyInfo->country=='IL') ? 'selected' : '' }}>Israel</option>
                        <option value="IT" {{ ($propertyInfo->country=='IT') ? 'selected' : '' }}>Italy</option>
                        <option value="JM" {{ ($propertyInfo->country=='JM') ? 'selected' : '' }}>Jamaica</option>
                        <option value="JP" {{ ($propertyInfo->country=='JP') ? 'selected' : '' }}>Japan</option>
                        <option value="JE" {{ ($propertyInfo->country=='JE') ? 'selected' : '' }}>Jersey</option>
                        <option value="JO" {{ ($propertyInfo->country=='JO') ? 'selected' : '' }}>Jordan</option>
                        <option value="KZ" {{ ($propertyInfo->country=='KZ') ? 'selected' : '' }}>Kazakhstan</option>
                        <option value="KE" {{ ($propertyInfo->country=='KE') ? 'selected' : '' }}>Kenya</option>
                        <option value="KI" {{ ($propertyInfo->country=='KI') ? 'selected' : '' }}>Kiribati</option>
                        <option value="KP" {{ ($propertyInfo->country=='KP') ? 'selected' : '' }}>Korea, Democratic People's Republic of</option>
                        <option value="KR" {{ ($propertyInfo->country=='KR') ? 'selected' : '' }}>Korea, Republic of</option>
                        <option value="KW" {{ ($propertyInfo->country=='KW') ? 'selected' : '' }}>Kuwait</option>
                        <option value="KG" {{ ($propertyInfo->country=='KG') ? 'selected' : '' }}>Kyrgyzstan</option>
                        <option value="LA" {{ ($propertyInfo->country=='LA') ? 'selected' : '' }}>Lao People's Democratic Republic</option>
                        <option value="LV" {{ ($propertyInfo->country=='LV') ? 'selected' : '' }}>Latvia</option>
                        <option value="LB" {{ ($propertyInfo->country=='LB') ? 'selected' : '' }}>Lebanon</option>
                        <option value="LS" {{ ($propertyInfo->country=='LS') ? 'selected' : '' }}>Lesotho</option>
                        <option value="LR" {{ ($propertyInfo->country=='LR') ? 'selected' : '' }}>Liberia</option>
                        <option value="LY" {{ ($propertyInfo->country=='LY') ? 'selected' : '' }}>Libya</option>
                        <option value="LI" {{ ($propertyInfo->country=='LI') ? 'selected' : '' }}>Liechtenstein</option>
                        <option value="LT" {{ ($propertyInfo->country=='LT') ? 'selected' : '' }}>Lithuania</option>
                        <option value="LU" {{ ($propertyInfo->country=='LU') ? 'selected' : '' }}>Luxembourg</option>
                        <option value="MO" {{ ($propertyInfo->country=='MO') ? 'selected' : '' }}>Macao</option>
                        <option value="MK" {{ ($propertyInfo->country=='MK') ? 'selected' : '' }}>Macedonia, the former Yugoslav Republic of</option>
                        <option value="MG" {{ ($propertyInfo->country=='MG') ? 'selected' : '' }}>Madagascar</option>
                        <option value="MW" {{ ($propertyInfo->country=='MW') ? 'selected' : '' }}>Malawi</option>
                        <option value="MY" {{ ($propertyInfo->country=='MY') ? 'selected' : '' }}>Malaysia</option>
                        <option value="MV" {{ ($propertyInfo->country=='MV') ? 'selected' : '' }}>Maldives</option>
                        <option value="ML" {{ ($propertyInfo->country=='ML') ? 'selected' : '' }}>Mali</option>
                        <option value="MT" {{ ($propertyInfo->country=='MT') ? 'selected' : '' }}>Malta</option>
                        <option value="MH" {{ ($propertyInfo->country=='MH') ? 'selected' : '' }}>Marshall Islands</option>
                        <option value="MQ" {{ ($propertyInfo->country=='MQ') ? 'selected' : '' }}>Martinique</option>
                        <option value="MR" {{ ($propertyInfo->country=='MR') ? 'selected' : '' }}>Mauritania</option>
                        <option value="MU" {{ ($propertyInfo->country=='MU') ? 'selected' : '' }}>Mauritius</option>
                        <option value="YT" {{ ($propertyInfo->country=='YT') ? 'selected' : '' }}>Mayotte</option>
                        <option value="MX" {{ ($propertyInfo->country=='MX') ? 'selected' : '' }}>Mexico</option>
                        <option value="FM" {{ ($propertyInfo->country=='FM') ? 'selected' : '' }}>Micronesia, Federated States of</option>
                        <option value="MD" {{ ($propertyInfo->country=='MD') ? 'selected' : '' }}>Moldova, Republic of</option>
                        <option value="MC" {{ ($propertyInfo->country=='MC') ? 'selected' : '' }}>Monaco</option>
                        <option value="MN" {{ ($propertyInfo->country=='MN') ? 'selected' : '' }}>Mongolia</option>
                        <option value="ME" {{ ($propertyInfo->country=='ME') ? 'selected' : '' }}>Montenegro</option>
                        <option value="MS" {{ ($propertyInfo->country=='MS') ? 'selected' : '' }}>Montserrat</option>
                        <option value="MA" {{ ($propertyInfo->country=='MA') ? 'selected' : '' }}>Morocco</option>
                        <option value="MZ" {{ ($propertyInfo->country=='MZ') ? 'selected' : '' }}>Mozambique</option>
                        <option value="MM" {{ ($propertyInfo->country=='MM') ? 'selected' : '' }}>Myanmar</option>
                        <option value="NA" {{ ($propertyInfo->country=='NA') ? 'selected' : '' }}>Namibia</option>
                        <option value="NR" {{ ($propertyInfo->country=='NR') ? 'selected' : '' }}>Nauru</option>
                        <option value="NP" {{ ($propertyInfo->country=='NP') ? 'selected' : '' }}>Nepal</option>
                        <option value="NL" {{ ($propertyInfo->country=='NL') ? 'selected' : '' }}>Netherlands</option>
                        <option value="NC" {{ ($propertyInfo->country=='NC') ? 'selected' : '' }}>New Caledonia</option>
                        <option value="NZ" {{ ($propertyInfo->country=='NZ') ? 'selected' : '' }}>New Zealand</option>
                        <option value="NI" {{ ($propertyInfo->country=='NI') ? 'selected' : '' }}>Nicaragua</option>
                        <option value="NE" {{ ($propertyInfo->country=='NE') ? 'selected' : '' }}>Niger</option>
                        <option value="NG" {{ ($propertyInfo->country=='NG') ? 'selected' : '' }}>Nigeria</option>
                        <option value="NU" {{ ($propertyInfo->country=='NU') ? 'selected' : '' }}>Niue</option>
                        <option value="NF" {{ ($propertyInfo->country=='NF') ? 'selected' : '' }}>Norfolk Island</option>
                        <option value="MP" {{ ($propertyInfo->country=='MP') ? 'selected' : '' }}>Northern Mariana Islands</option>
                        <option value="NO" {{ ($propertyInfo->country=='NO') ? 'selected' : '' }}>Norway</option>
                        <option value="OM" {{ ($propertyInfo->country=='OM') ? 'selected' : '' }}>Oman</option>
                        <option value="PK" {{ ($propertyInfo->country=='PK') ? 'selected' : '' }}>Pakistan</option>
                        <option value="PW" {{ ($propertyInfo->country=='PW') ? 'selected' : '' }}>Palau</option>
                        <option value="PS" {{ ($propertyInfo->country=='PS') ? 'selected' : '' }}>Palestinian Territory, Occupied</option>
                        <option value="PA" {{ ($propertyInfo->country=='PA') ? 'selected' : '' }}>Panama</option>
                        <option value="PG" {{ ($propertyInfo->country=='PG') ? 'selected' : '' }}>Papua New Guinea</option>
                        <option value="PY" {{ ($propertyInfo->country=='PY') ? 'selected' : '' }}>Paraguay</option>
                        <option value="PE" {{ ($propertyInfo->country=='PE') ? 'selected' : '' }}>Peru</option>
                        <option value="PH" {{ ($propertyInfo->country=='PH') ? 'selected' : '' }}>Philippines</option>
                        <option value="PN" {{ ($propertyInfo->country=='PN') ? 'selected' : '' }}>Pitcairn</option>
                        <option value="PL" {{ ($propertyInfo->country=='PL') ? 'selected' : '' }}>Poland</option>
                        <option value="PT" {{ ($propertyInfo->country=='PT') ? 'selected' : '' }}>Portugal</option>
                        <option value="PR" {{ ($propertyInfo->country=='PR') ? 'selected' : '' }}>Puerto Rico</option>
                        <option value="QA" {{ ($propertyInfo->country=='QA') ? 'selected' : '' }}>Qatar</option>
                        <option value="RE" {{ ($propertyInfo->country=='RE') ? 'selected' : '' }}>Réunion</option>
                        <option value="RO" {{ ($propertyInfo->country=='RO') ? 'selected' : '' }}>Romania</option>
                        <option value="RU" {{ ($propertyInfo->country=='RU') ? 'selected' : '' }}>Russian Federation</option>
                        <option value="RW" {{ ($propertyInfo->country=='RW') ? 'selected' : '' }}>Rwanda</option>
                        <option value="BL" {{ ($propertyInfo->country=='BL') ? 'selected' : '' }}>Saint Barthélemy</option>
                        <option value="SH" {{ ($propertyInfo->country=='SH') ? 'selected' : '' }}>Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="KN" {{ ($propertyInfo->country=='KN') ? 'selected' : '' }}>Saint Kitts and Nevis</option>
                        <option value="LC" {{ ($propertyInfo->country=='LC') ? 'selected' : '' }}>Saint Lucia</option>
                        <option value="MF" {{ ($propertyInfo->country=='MF') ? 'selected' : '' }}>Saint Martin (French part)</option>
                        <option value="PM" {{ ($propertyInfo->country=='PM') ? 'selected' : '' }}>Saint Pierre and Miquelon</option>
                        <option value="VC" {{ ($propertyInfo->country=='VC') ? 'selected' : '' }}>Saint Vincent and the Grenadines</option>
                        <option value="WS" {{ ($propertyInfo->country=='WS') ? 'selected' : '' }}>Samoa</option>
                        <option value="SM" {{ ($propertyInfo->country=='SM') ? 'selected' : '' }}>San Marino</option>
                        <option value="ST" {{ ($propertyInfo->country=='ST') ? 'selected' : '' }}>Sao Tome and Principe</option>
                        <option value="SA" {{ ($propertyInfo->country=='SA') ? 'selected' : '' }}>Saudi Arabia</option>
                        <option value="SN" {{ ($propertyInfo->country=='SN') ? 'selected' : '' }}>Senegal</option>
                        <option value="RS" {{ ($propertyInfo->country=='RS') ? 'selected' : '' }}>Serbia</option>
                        <option value="SC" {{ ($propertyInfo->country=='SC') ? 'selected' : '' }}>Seychelles</option>
                        <option value="SL" {{ ($propertyInfo->country=='SL') ? 'selected' : '' }}>Sierra Leone</option>
                        <option value="SG" {{ ($propertyInfo->country=='SG') ? 'selected' : '' }}>Singapore</option>
                        <option value="SX" {{ ($propertyInfo->country=='SX') ? 'selected' : '' }}>Sint Maarten (Dutch part)</option>
                        <option value="SK" {{ ($propertyInfo->country=='SK') ? 'selected' : '' }}>Slovakia</option>
                        <option value="SI" {{ ($propertyInfo->country=='SI') ? 'selected' : '' }}>Slovenia</option>
                        <option value="SB" {{ ($propertyInfo->country=='SB') ? 'selected' : '' }}>Solomon Islands</option>
                        <option value="SO" {{ ($propertyInfo->country=='SO') ? 'selected' : '' }}>Somalia</option>
                        <option value="ZA" {{ ($propertyInfo->country=='ZA') ? 'selected' : '' }}>South Africa</option>
                        <option value="GS" {{ ($propertyInfo->country=='GS') ? 'selected' : '' }}>South Georgia and the South Sandwich Islands</option>
                        <option value="SS" {{ ($propertyInfo->country=='SS') ? 'selected' : '' }}>South Sudan</option>
                        <option value="ES" {{ ($propertyInfo->country=='ES') ? 'selected' : '' }}>Spain</option>
                        <option value="LK" {{ ($propertyInfo->country=='LK') ? 'selected' : '' }}>Sri Lanka</option>
                        <option value="SD" {{ ($propertyInfo->country=='SD') ? 'selected' : '' }}>Sudan</option>
                        <option value="SR" {{ ($propertyInfo->country=='SR') ? 'selected' : '' }}>Suriname</option>
                        <option value="SJ" {{ ($propertyInfo->country=='SJ') ? 'selected' : '' }}>Svalbard and Jan Mayen</option>
                        <option value="SZ" {{ ($propertyInfo->country=='SZ') ? 'selected' : '' }}>Swaziland</option>
                        <option value="SE" {{ ($propertyInfo->country=='SE') ? 'selected' : '' }}>Sweden</option>
                        <option value="CH" {{ ($propertyInfo->country=='CH') ? 'selected' : '' }}>Switzerland</option>
                        <option value="SY" {{ ($propertyInfo->country=='SY') ? 'selected' : '' }}>Syrian Arab Republic</option>
                        <option value="TW" {{ ($propertyInfo->country=='TW') ? 'selected' : '' }}>Taiwan, Province of China</option>
                        <option value="TJ" {{ ($propertyInfo->country=='TJ') ? 'selected' : '' }}>Tajikistan</option>
                        <option value="TZ" {{ ($propertyInfo->country=='TZ') ? 'selected' : '' }}>Tanzania, United Republic of</option>
                        <option value="TH" {{ ($propertyInfo->country=='TH') ? 'selected' : '' }}>Thailand</option>
                        <option value="TL" {{ ($propertyInfo->country=='TL') ? 'selected' : '' }}>Timor-Leste</option>
                        <option value="TG" {{ ($propertyInfo->country=='TG') ? 'selected' : '' }}>Togo</option>
                        <option value="TK" {{ ($propertyInfo->country=='TK') ? 'selected' : '' }}>Tokelau</option>
                        <option value="TO" {{ ($propertyInfo->country=='TO') ? 'selected' : '' }}>Tonga</option>
                        <option value="TT" {{ ($propertyInfo->country=='TT') ? 'selected' : '' }}>Trinidad and Tobago</option>
                        <option value="TN" {{ ($propertyInfo->country=='TN') ? 'selected' : '' }}>Tunisia</option>
                        <option value="TR" {{ ($propertyInfo->country=='TR') ? 'selected' : '' }}>Turkey</option>
                        <option value="TM" {{ ($propertyInfo->country=='TM') ? 'selected' : '' }}>Turkmenistan</option>
                        <option value="TC" {{ ($propertyInfo->country=='TC') ? 'selected' : '' }}>Turks and Caicos Islands</option>
                        <option value="TV" {{ ($propertyInfo->country=='TV') ? 'selected' : '' }}>Tuvalu</option>
                        <option value="UG" {{ ($propertyInfo->country=='UG') ? 'selected' : '' }}>Uganda</option>
                        <option value="UA" {{ ($propertyInfo->country=='UA') ? 'selected' : '' }}>Ukraine</option>
                        <option value="AE" {{ ($propertyInfo->country=='AE') ? 'selected' : '' }}>United Arab Emirates</option>
                        <option value="GB" {{ ($propertyInfo->country=='GB') ? 'selected' : '' }}>United Kingdom</option>
                        <option value="US" {{ ($propertyInfo->country=='US') ? 'selected' : '' }}>United States</option>
                        <option value="UM" {{ ($propertyInfo->country=='UM') ? 'selected' : '' }}>United States Minor Outlying Islands</option>
                        <option value="UY" {{ ($propertyInfo->country=='UY') ? 'selected' : '' }}>Uruguay</option>
                        <option value="UZ" {{ ($propertyInfo->country=='UZ') ? 'selected' : '' }}>Uzbekistan</option>
                        <option value="VU" {{ ($propertyInfo->country=='VU') ? 'selected' : '' }}>Vanuatu</option>
                        <option value="VE" {{ ($propertyInfo->country=='VE') ? 'selected' : '' }}>Venezuela, Bolivarian Republic of</option>
                        <option value="VN" {{ ($propertyInfo->country=='VN') ? 'selected' : '' }}>Viet Nam</option>
                        <option value="VG" {{ ($propertyInfo->country=='VG') ? 'selected' : '' }}>Virgin Islands, British</option>
                        <option value="VI" {{ ($propertyInfo->country=='VI') ? 'selected' : '' }}>Virgin Islands, U.S.</option>
                        <option value="WF" {{ ($propertyInfo->country=='WF') ? 'selected' : '' }}>Wallis and Futuna</option>
                        <option value="EH" {{ ($propertyInfo->country=='EH') ? 'selected' : '' }}>Western Sahara</option>
                        <option value="YE" {{ ($propertyInfo->country=='YE') ? 'selected' : '' }}>Yemen</option>
                        <option value="ZM" {{ ($propertyInfo->country=='ZM') ? 'selected' : '' }}>Zambia</option>
                        <option value="ZW" {{ ($propertyInfo->country=='ZW') ? 'selected' : '' }}>Zimbabwe</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="city">City <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" id="city" name="city" placeholder="city" value="{{ old('city', $propertyInfo->city) }}">
                    @if($errors->has('city'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('city') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="mindaysstay">Minimum days stay</label>
                    <input type="number" class="form-control" id="mindaysstay" name="mindaysstay" placeholder="0" value="{{ old('mindaysstay', $propertyInfo->mindaysstay) }}">
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="address">Address <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" id="address" name="address" placeholder="Address" value="{{ old('address', $propertyInfo->address) }}">
                    @if($errors->has('address'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('address') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="checkin">Check-in time</label>
                    <input type="time" class="form-control" id="checkin" name="checkin" min="14:00" value="{{ old('checkin', $propertyInfo->checkin) }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="checkout">Check-out time</label>
                    <input type="time" class="form-control" id="checkout" name="checkout" min="00:00" value="{{ old('checkout', $propertyInfo->checkout) }}">
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="postalcode">Postal Code <span class="text-danger">*</span></label>
                    <input type="number" class="form-control {{ $errors->has('postalcode') ? 'is-invalid' : '' }}" id="postalcode" name="postalcode" value="{{ old('postalcode', $propertyInfo->postalcode) }}">
                    @if($errors->has('postalcode'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('postalcode') }}</strong>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="tandc">Terms & Conditions URL</label>
                    <input type="text" class="form-control" id="tandc" name="tandc" placeholder="URL" value="{{ old('tandc', $propertyInfo->tandc) }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="otherinfo">Other Info</label>
                    <textarea class="form-control" id="otherinfo" name="otherinfo" rows="4">{{ old('otherinfo', $propertyInfo->otherinfo) }}</textarea>
                    </div>
                </div>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
