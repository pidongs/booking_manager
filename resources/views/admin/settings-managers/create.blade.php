@extends('layouts.master')

@section('title', 'Add Manager | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Add Manager </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item"><a href="#">Managers</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Manager</li>
        </ol>
        </nav>
    </div>
    <div class="row">

        <!--FORM-->
        <div class="col-12">
        <div class="card">
            <div class="card-body">
            @include('layouts._messages')
            <form class="form-sample" action="{{ route('managers.store') }}" method="POST">
                @include('admin.settings-managers._form', ['buttonText' => 'Save'])
            </form>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
