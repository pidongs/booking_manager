@extends('layouts.master')

@push('head')
<link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
/* Toggle Button */
.cm-toggle {
	-webkit-appearance: none;
	-webkit-tap-highlight-color: transparent;
	position: relative;
	border: 0;
	outline: 0;
	cursor: pointer;
	margin: 10px;
}

/* To create surface of toggle button */
.cm-toggle:after {
	content: '';
	width: 40px;
	height: 18px;
	display: inline-block;
	background: rgba(196, 195, 195, 0.55);
	border-radius: 18px;
	clear: both;
}

/* Contents before checkbox to create toggle handle */
.cm-toggle:before {
	content: '';
	width: 21px;
	height: 21px;
	display: block;
	position: absolute;
	left: 0;
	top: -2px;
	border-radius: 50%;
	background: rgb(255, 255, 255);
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
}

/* Shift the handle to left on check event */
.cm-toggle:checked:before {
	left: 22px;
	box-shadow: -1px 1px 3px rgba(0, 0, 0, 0.6);
}
/* Background color when toggle button will be active */
.cm-toggle:checked:after {
	background: #16a085;
}

/* Transition for smoothness */
.cm-toggle,
.cm-toggle:before,
.cm-toggle:after,
.cm-toggle:checked:before,
.cm-toggle:checked:after {
	transition: ease .3s;
	-webkit-transition: ease .3s;
	-moz-transition: ease .3s;
	-o-transition: ease .3s;
}

.dataTables_info  {
    font-size: small;
}

.toolbar {
    float: right;
}

</style>
@endpush

@section('title', 'Managers | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Managers </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Managers</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                {{-- <managers></managers> --}}

                <table class="table table-responsive mb-4 dt">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Email</th>
                        <th>Owner</th>
                        <th>Bookings</th>
                        <th>Admin</th>
                        <th>Calendar</th>
                        <th>CC</th>
                        <th>Channel Manager</th>
                        <th>Settings</th>
                        <th>Delete</th>
                        <th>Print</th>
                        <th>Date Created</th>
                        <th>Created IP</th>
                        <th>Last Login</th>
                        <th>Last IP</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(function() {
    var table = $('.dt').DataTable({
        processing: true,
        autoWidth: false,
        ajax: {
            url: "{{ route('managers.allManagers') }}",
            type: "GET"
        },
        language: { search: "", sLengthMenu: "_MENU_" },
        dom: "<'toolbar'>frt<'row'<'ml-4'l><'col-sm-4'i>>",
        initComplete: function(){
            $("div.toolbar").html("<a href='{{ route('managers.create') }}' class='btn btn-gradient-info btn-icon-text ml-3'><i class='mdi mdi-plus'></i> CREATE </a>");
        },
        columns: [
                { data: 'title', name: 'title', orderable: true },
                { data: 'email', name: 'email', orderable: true },
                { data: 'owner_rights', name: 'owner_rights', orderable: false },
                { data: 'booking_rights', name: 'booking_rights', orderable: false },
                { data: 'admin_rights', name: 'admin_rights', orderable: false },
                { data: 'calendar_rights', name: 'calendar_rights', orderable: false },
                { data: 'cc_rights', name: 'cc_rights', orderable: false },
                { data: 'channelmanager_rights', name: 'channelmanager_rights', orderable: false },
                { data: 'settings_rights', name: 'settings_rights', orderable: false },
                { data: 'delete_rights', name: 'delete_rights', orderable: false },
                { data: 'print_rights', name: 'print_rights', orderable: false },
                { data: 'created_at', name: 'crated_at', orderable: false },
                { data: 'created_ip', name: 'created_ip', orderable: false },
                { data: 'last_login', name: 'last_login', orderable: false },
                { data: 'last_ip', name: 'last_ip', orderable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        "lengthMenu": [[10, 20, 30, 50, 100], [10, 20, 30, 50, 100]],
        "infoCallback": function( settings, start, end, max, total, pre ) {
            return "Showing " + end + " - " + end + " of " + total;
        },
        'columnDefs': [
        {
            "targets": "_all",
            "width": "20%",
        }]
    });

    $('div.dataTables_filter input[type="search"]').addClass('form-control mb-4');

    $('div.dataTables_length select').addClass('form-control');

    $('.dataTables_filter input[type="search"]')
        .attr('placeholder','Search...');

    $('.dt tbody').on('click', '.deleteManager', function (e) {
        e.preventDefault();
        swal({
            text: "Are you sure you want to delete this record?",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Close",
                    value: null,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "btn btn-primary",
                    closeModal: true
                }
            }
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('form').submit();
            }
        })
        .catch((error) => {
            swal('Error', 'Manager has not been deleted.', 'error');
        })
    });

    $('.dt tbody').on('change', '.owner', function (e) {
        var val = $(this).parent().find("input[name='owner_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.booking', function (e) {
        var val = $(this).parent().find("input[name='booking_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    // $('.dt tbody').on('change', '.datepicker', function (e) {
    //     var val = $(this).parent().find("input[name='datepicker_rights']");
    //     if($(this).is(':checked')){
    //         val.val('1');
    //         $(this).closest('form').submit();
    //     }
    //     else{
    //         $(this).closest('form').submit();
    //     }
    // });

    $('.dt tbody').on('change', '.admin', function (e) {
        var val = $(this).parent().find("input[name='admin_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.calendar', function (e) {
        var val = $(this).parent().find("input[name='calendar_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.cc', function (e) {
        var val = $(this).parent().find("input[name='cc_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.cm', function (e) {
        var val = $(this).parent().find("input[name='channelmanager_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.settings', function (e) {
        var val = $(this).parent().find("input[name='settings_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.delete', function (e) {
        var val = $(this).parent().find("input[name='delete_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

    $('.dt tbody').on('change', '.print', function (e) {
        var val = $(this).parent().find("input[name='print_rights']");
        if($(this).is(':checked')){
            val.val('1');
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('form').submit();
        }
    });

});

</script>
@endpush
