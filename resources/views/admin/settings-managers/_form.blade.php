@csrf
<!--BUTTON SAVE-->
<div class="pull-right">
    <button type="submit" class="btn btn-gradient-primary btn-icon-text">
    <i class="mdi mdi-file-check btn-icon-prepend"></i> Save </button>
</div>

<p class="card-description"> Manager </p>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="title'">Title <span class="text-danger">*</span></label>
            <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title" name="title" value="{{ old('title', $user->title) }}" maxlength="100">
            @if($errors->has('title'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('title') }}</strong>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="email">Email (Username) <span class="text-danger">*</span></label>
            <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $user->email) }}">
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" name="password">
            @if($errors->has('password'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="date_created">Date Created</label>
        <input type="text" class="form-control" value="{{ $user->created_at }}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="created_ip">Created IP</label>
        <input type="text" class="form-control" value="{{ $user->created_ip }}" readonly>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="last_login">Last Login</label>
        <input type="text" class="form-control" value="{{ $user->last_login }}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="last_login_ip">Last Login IP</label>
        <input type="text" class="form-control" value="{{ $user->last_ip }}" readonly>
        </div>
    </div>
</div>
