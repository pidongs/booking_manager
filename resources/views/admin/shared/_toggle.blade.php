
<form method="POST" action="{{ route($route, ['id' => $model->id, 'field' => $field]) }}">
    @csrf
    <input type="text" hidden value="{{ $model->email }}" name="email">
    <input type="text" hidden value="0" name="{{ $field }}">
    <input type="checkbox" class="cm-toggle {{ $name }}" name="{{ $name }}" {{ ($model->$field) ? 'checked' : '' }}>
</form>
