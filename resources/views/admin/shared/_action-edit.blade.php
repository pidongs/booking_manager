<a href="{{ route($route, $model->id) }}" class="btn btn-link btn-sm text-info" title="View this record"><i class="fa fa-search"></i> </a>

<a href="{{ route($route1, $model->id) }}" class="btn btn-link btn-sm text-success" title="Edit this record"><i class="fa fa-pencil-square-o"></i></a>
