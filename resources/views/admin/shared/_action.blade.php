
<a href="{{ route($route1, $model->id) }}" class="btn btn-link btn-sm mb-1 text-success" title="Edit this record"><i class="fa fa-pencil-square-o"></i></a>

<form method="POST" action="{{ route($route2, $model->id) }}">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-link btn-sm text-danger {{ $buttonId }}" title="Delete this record"><i class="fa fa-trash-o"></i></button>
</form>
