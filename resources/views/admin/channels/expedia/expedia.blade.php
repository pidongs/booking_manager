@extends('layouts.master')

@section('title', 'Expedia Channel Manager | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Expedia Channel Manager </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Channels</a></li>
            <li class="breadcrumb-item active" aria-current="page">Expedia Channel Manager</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <expedia></expedia>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
