@extends('layouts.master')

@section('title', 'Map Expedia | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-multiple"></i>
        </span> Map Expedia </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Channels</a></li>
            <li class="breadcrumb-item"><a href="#">Expedia</a></li>
            <li class="breadcrumb-item active" aria-current="page">Map Expedia</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <h4 class="card-title mb-4">Map Expedia Property IDs</h4>
                <hr><br>

                <expediamapping roomname="{{ $room->name }}" id="{{ $id }}"></expediamapping>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
