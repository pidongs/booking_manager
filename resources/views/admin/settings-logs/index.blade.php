@extends('layouts.master')

@push('head')
<link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
/* Toggle Button */
.cm-toggle {
	-webkit-appearance: none;
	-webkit-tap-highlight-color: transparent;
	position: relative;
	border: 0;
	outline: 0;
	cursor: pointer;
	margin: 10px;
}


/* To create surface of toggle button */
.cm-toggle:after {
	content: '';
	width: 50px;
	height: 18px;
	display: inline-block;
	background: rgba(196, 195, 195, 0.55);
	border-radius: 18px;
	clear: both;
}


/* Contents before checkbox to create toggle handle */
.cm-toggle:before {
	content: '';
	width: 22px;
	height: 22px;
	display: block;
	position: absolute;
	left: 0;
	top: -2px;
	border-radius: 50%;
	background: rgb(255, 255, 255);
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
}


/* Shift the handle to left on check event */
.cm-toggle:checked:before {
	left: 32px;
	box-shadow: -1px 1px 3px rgba(0, 0, 0, 0.6);
}
/* Background color when toggle button will be active */
.cm-toggle:checked:after {
	background: #16a085;
}

/* Transition for smoothness */
.cm-toggle,
.cm-toggle:before,
.cm-toggle:after,
.cm-toggle:checked:before,
.cm-toggle:checked:after {
	transition: ease .3s;
	-webkit-transition: ease .3s;
	-moz-transition: ease .3s;
	-o-transition: ease .3s;
}

.dataTables_info, .dataTables_paginate  {
    font-size: small;
}

table.dataTable tbody tr td {
    word-break: break-word;
}

.dataTables_wrapper .dataTables_filter {
    float: left !important;
}

.daterange, .set {
    float: left;
}

.set{
    margin-left: 25px;
}

</style>
@endpush

@section('title', 'Logs | Booking Manager')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-file-document"></i>
        </span> Logs </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Logs</li>
        </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                @include('layouts._messages')

                <input type="hidden" id="start" name="start_date">
                <input type="hidden" id="end" name="end_date">
                <input type="text" class="form-control" style="display: hidden;" id="daterange" name="daterange">

                <table class="mb-4 dt">
                    <thead>
                    <tr>
                        <th>Created</th>
                        <th>User</th>
                        <th>Exception</th>
                        <th>Info</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(function() {
    $('#start').val(moment().format('MM/DD/YYYY'));
    $('#end').val(moment().add(31, 'days').format('MM/DD/YYYY'));

    $('#daterange').daterangepicker({
        "autoApply": true,
        "startDate": moment().format('MM/DD/YYYY'),
        "endDate": moment().add(31, 'days').format('MM/DD/YYYY')
    }, function(start, end) {
        $('#start').val(start.format('MM/DD/YYYY'));
        $('#end').val(end.format('MM/DD/YYYY'));
        $('.dt').DataTable().draw(true);
    });

    var table = $('.dt').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ajax: {
            url: "{{ route('logs.index') }}",
            type: 'GET',
            data: function (d) {
                d.start_date = $('#start').val();
                d.end_date = $('#end').val();
            }
        },
        language: { search: "", sLengthMenu: "_MENU_" },
        dom: "f<'daterange'>rt<'row'<'ml-4 mt-4'l><'col-sm-4 mt-4'i>>",
        initComplete: function(){
            $("div.daterange").html($("#daterange"));
        },
        columns: [
                { data: 'created_at', name: 'created_at' },
                { data: 'user', name: 'user' },
                { data: 'description', name: 'description', orderable: false },
                { data: 'user_agent', name: 'user_agent', orderable: false },
        ],
        "lengthMenu": [[10, 20, 30, 50, 100], [10, 20, 30, 50, 100]],
        "infoCallback": function( settings, start, end, max, total, pre ) {
            return "Showing " + end + " - " + end + " of " + total;
        },
        order: [[0, 'desc']],
        'columnDefs': [
        {
            "targets": [2],
            "width": "20%",
        },
        {
            "targets": [3],
            "width": "30%",
        },
        {
            "targets": 0,
            "width": "15%",
        }],
    });

    $('.dataTables_filter input[type="search"]').addClass('form-control mb-4');

    $('div.dataTables_length select').addClass('form-control');

    $('#daterange').addClass('ml-3');

    $('.dataTables_filter input[type="search"]')
        .attr('placeholder','Search...');

});

</script>
@endpush
