<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route for Index / Login
Route::get('/', function () {
    return redirect()->route('login');
});

//Route for Authentication
Auth::routes();

//Use Auth middleware
Route::group(['middleware' => 'auth'], function () {
    //Routes for Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/todayAvailable', 'DashboardController@todayAvailable')->name('todayAvailable');
    Route::get('/tomorrowAvailable', 'DashboardController@tomorrowAvailable')->name('tomorrowAvailable');
    Route::get('/availableToday', 'DashboardController@availableToday')->name('availableToday');
    Route::get('/availableTomorrow', 'DashboardController@availableTomorrow')->name('availableTomorrow');
    Route::get('/dataBookings', 'DashboardController@dataBookings')->name('bookings.dataBookings');

    //Route for Payment Info
    Route::get('/paymentInfo/{id}/editPaymentInfo', 'PaymentInfoController@editPaymentInfo')->name('editPaymentInfo');
    Route::post('/paymentInfo/{id}/updatePaymentInfo', 'PaymentInfoController@updatePaymentInfo')->name('paymentinfo.update');

    //Routes for Rooms
    Route::resource('rooms', 'RoomsController')->names([
        'index' => 'rooms.index',
        'store' => 'rooms.store',
        'update' => 'rooms.update',
        'destroy' => 'rooms.destroy',
    ]);
    Route::get('/createRoom', 'RoomsController@createRoom')->name('rooms.create');
    Route::get('/rooms/{id}/editRoomInfo', 'RoomsController@editRoomInfo')->name('rooms.edit');
    Route::post('/rooms/{id}/update', 'UpdateBookableController')->name('rooms.updateBookable');
    Route::get('/dt', 'RoomsController@dt')->name('dt');
    Route::get('/sampleDt', 'RoomsController@sampleDt')->name('sampleDt');

    //Routes for Invoice Info
    Route::get('/invoiceInfo/{id}/editInvoiceInfo', 'InvoiceInfoController@editInvoiceInfo')->name('editInvoiceInfo');
    Route::post('/invoiceInfo/{id}/updateInvoiceInfo', 'InvoiceInfoController@updateInvoiceInfo')->name('invoiceinfo.update');

    //Routes for Property Info
    Route::get('/propertyInfo/{id}/editPropertyInfo', 'PropertyInfoController@editPropertyInfo')->name('editPropertyInfo');
    Route::post('/propertyInfo/{id}/updatePropertyInfo', 'PropertyInfoController@updatePropertyInfo')->name('propertyinfo.update');

    //Route for Logs
    Route::get('/logs', 'LogsController@logActivity')->name('logs.index');
    Route::get('/markAllRead/{user}', 'LogsController@markAllRead');

    //Routes for Managers
    Route::resource('managers', 'ManagersController')->names([
        'index' => 'managers.index',
        'store' => 'managers.store',
        'destroy' => 'managers.destroy'
    ]);
    Route::get('/managers/{id}/editManagerInfo', 'ManagersController@editManagerInfo')->name('managers.edit');
    Route::get('/allManagers', 'ManagersController@getAllManagers')->name('managers.allManagers');
    Route::get('/getManagers', 'ManagersController@getManagers')->name('managers.getManagers');
    Route::get('/createManager', 'ManagersController@createManager')->name('managers.create');
    Route::post('/managers/{id}/updateManagerInfo', 'ManagersController@updateManagerInfo')->name('managers.update');
    Route::post('/managers/{id}/{field}/updateRights', 'UpdateRightsController@updateRights')->name('manager.updateRights');
    // Route::delete('/managers/{id}/deleteManager', 'ManagersController@destroy')->name('managers.destroy');
    // Route::post('/updateRightsManager/{id}/{column}/{value}', 'UpdateRightsController@updateRightsManager')->name('manager.updateRightsManager');
    // Route::post('/managers/{id}/updateBookingRights', 'UpdateRightsController@updateBookingRights')->name('manager.updateBookingRights');
    // Route::post('/managers/{id}/updateDatepickerRights', 'UpdateRightsController@updateDatepickerRights')->name('manager.updateDatepickerRights');
    // Route::post('/managers/{id}/updateAdminRights', 'UpdateRightsController@updateAdminRights')->name('manager.updateAdminRights');
    // Route::post('/managers/{id}/updateCalendarRights', 'UpdateRightsController@updateCalendarRights')->name('manager.updateCalendarRights');
    // Route::post('/managers/{id}/updateCcRights', 'UpdateRightsController@updateCcRights')->name('manager.updateCcRights');
    // Route::post('/managers/{id}/updateChannelManagerRights', 'UpdateRightsController@updateChannelManagerRights')->name('manager.updateChannelManagerRights');
    // Route::post('/managers/{id}/updateSettingsRights', 'UpdateRightsController@updateSettingsRights')->name('manager.updateSettingsRights');
    // Route::post('/managers/{id}/updateDeleteRights', 'UpdateRightsController@updateDeleteRights')->name('manager.updateDeleteRights');
    // Route::post('/managers/{id}/updatePrintRights', 'UpdateRightsController@updatePrintRights')->name('manager.updatePrintRights');

    //Route for Bookings
    Route::resource('bookings', 'BookingsController')->names([
        'store' => 'bookings.store',
        'update' => 'bookings.update',
        'destroy' => 'bookings.destroy',
    ]);
    Route::get('/bookings/{id}/editBooking', 'BookingsController@editBooking')->name('bookings.edit');
    Route::get('/newBooking', 'BookingsController@newBooking')->name('bookings.create');
    Route::get('/bookings/{id}/new', 'BookingsController@createBooking')->name('bookings.new');
    Route::get('/todayCheckin', 'BookingsController@todayCheckin')->name('bookings.todayCheckin');
    Route::get('/todayCheckout', 'BookingsController@todayCheckout')->name('bookings.todayCheckout');
    Route::get('/tomorrowCheckin', 'BookingsController@tomorrowCheckin')->name('bookings.tomorrowCheckin');
    Route::get('/tomorrowCheckout', 'BookingsController@tomorrowCheckout')->name('bookings.tomorrowCheckout');
    Route::post('/getPriceTotal', 'BookingsController@getPriceTotal')->name('getPriceTotal');
    Route::get('/calendar', 'BookingsController@calendar')->name('bookings.calendar');
    Route::get('/getBookings', 'BookingsController@getBookings')->name('bookings.getBookings');
    Route::get('/bookings/{id}/viewBooking', 'BookingsController@viewBooking')->name('bookings.viewBooking');
    Route::post('/changeStatus/{id}', 'BookingsController@changeStatus')->name('changeStatus');

    //Routes for Availability Calendar
    Route::get('/availabilityCalendar', 'AvailabilityController@availabilityCalendar')->name('availabilityCalendar');
    Route::get('/availabilityCalendar/dates/{from}/{to}', 'AvailabilityController@availabilityCalendarDate')->name('availabilityCalendarDate');
    Route::post('/availabilityGetDates', 'AvailabilityController@availabilityGetDates')->name('availabilityGetDates');
    Route::get('/getTableColumns', 'AvailabilityController@getTableColumns')->name('getTableColumns');
    Route::get('/data', 'AvailabilityController@data')->name('data');
    Route::get('/getRates', 'AvailabilityController@getRates')->name('getRates');
    Route::post('/dataAvailability', 'AvailabilityController@dataAvailability')->name('dataAvailability');
    Route::get('/set/{room}/{date}/{option}/{value}/availRates', 'AvailabilityController@setAvailabilityRates')->name('setAvailabilityRates');
    Route::get('/setNewAvailabilityRates', 'AvailabilityController@setNewAvailabilityRates')->name('setNewAvailabilityRates');
    Route::post('/updateAvailabilityRates', 'AvailabilityController@updateAvailabilityRates')->name('updateAvailabilityRates');

    //Routes for Channels
    Route::get('/expedia', 'ExpediaController@expedia')->name('expedia');
    Route::get('/getRooms', 'ExpediaController@getRooms')->name('getRooms');
    Route::get('/expedia/map/{id}', 'ExpediaController@map')->name('expedia.map');
    Route::post('/expedia/mapRoom/{id}', 'ExpediaController@mapRoom')->name('expedia.mapRoom');
    Route::post('/expedia/updateRoom/{id}/{propertyId}/{roomId}/{rateId}', 'ExpediaController@updateRoom')->name('expedia.updateRoom');
});
