<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'title',
        'owner_rights',
        'booking_rights',
        'datepicker_rights',
        'admin_rights',
        'calendar_rights',
        'cc_rights',
        'channelmanager_rights',
        'settings_rights',
        'delete_rights',
        'print_rights',
        'created_ip',
        'last_ip',
        'last_login',
        'invoice_info_id',
        'payment_info_id',
        'property_info_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
