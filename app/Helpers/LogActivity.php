<?php

namespace App\Helpers;

use App\Logs as LogActivityModel;
use App\Notifications\AllLogs;
use App\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;

class LogActivity
{
    public static function addToLog($description)
    {
        $log = [];
        $log['description'] = $description;
        $log['ip'] = Request::ip();
        $log['user_agent'] = Request::header('user-agent');
        $log['user'] = auth()->check() ? auth()->user()->email : 1;
        $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
        LogActivityModel::create($log);

        // $admins = User::where('admin_rights', 1)->get();
        // Notification::send($admins, new AllLogs($log));
    }

    public static function logActivityLists()
    {
        return LogActivityModel::where('user_id', auth()->user()->id)->get();
    }
}
