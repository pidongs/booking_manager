<?php

namespace App\Http\Controllers;

use App\User;
use Datatables;
use \App\Helpers\LogActivity;
use App\Logs;
use Carbon\Carbon;

class LogsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logActivity()
    {
        if (request()->ajax()) {
            $start_date = Carbon::parse($_GET["start_date"])->toDateString() . ' 00:00:00';
            $end_date = Carbon::parse($_GET["end_date"])->toDateString() . ' 23:59:59';
            $logs = Logs::where('user_id', auth()->user()->id)->whereBetween('created_at', [$start_date, $end_date])->get();
            return Datatables::of($logs)
                ->editColumn('user_agent', function (Logs $logs) {
                    return $logs->ip . '<br>' . $logs->user_agent;
                })
                ->rawColumns(['user_agent'])
                ->make(true);
        }
        return view('admin.settings-logs.index');
    }

    public function markAllRead(User $user)
    {
        $user->unreadNotifications->markAsRead();
    }
}
