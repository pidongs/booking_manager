<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\Room;
use Illuminate\Http\Request;

class UpdateBookableController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room->bookable = $request->bookable;
        $room->save();

        LogActivity::addToLog('Room updated successfully: ' . $request->roomName . ' (bookable=' . $request->bookable . ')');

        return redirect()->route('rooms.index')->with('success', 'Room updated successfully.');
    }
}
