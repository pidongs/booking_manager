<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\LogActivity;
use App\Http\Controllers\Controller;
use App\InvoiceInfo;
use App\PaymentInfo;
use App\PropertyInfo;
use App\Rules\GoogleRecaptcha;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd($data);
        $messages = [
            'property_name.required' => 'This field is required.',
            'property_address.required' => 'This field is required.',
            'postal_code.required' => 'This field is required.',
            'property_city.required' => 'This field is required.',
            'property_desc.required' => 'This field is required.',
            'company_details.required' => 'This field is required.',
            'property_desc.min' => 'Please enter at least 20 characters.',
            'company_details.min' => 'Please enter at least 20 characters.',
            'property_telnum.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'password.required' => 'This field is required.',
            'email.email' => 'Invalid email. Email must have "@" symbol and email domain ".com"',
            'g-recaptcha-response.required' => 'Please ensure that you are a human!'
        ];

        return Validator::make($data, [
            'property_name' => ['required', 'string', 'max:255'],
            'property_address' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string'],
            'property_city' => ['required', 'string'],
            'property_desc' => ['required', 'string', 'min:20', 'max: 255'],
            'company_details' => ['required', 'string', 'min:20', 'max: 255'],
            'property_telnum' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'g-recaptcha-response' => ['required', new GoogleRecaptcha]
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['property_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function registered(Request $request, $user)
    {
        //Create Property Info
        $propertyInfo = new PropertyInfo();
        $propertyInfo->title = $request->property_name;
        $propertyInfo->phone = $request->property_telnum;
        $propertyInfo->type = $request->type;
        $propertyInfo->email = $request->email;
        $propertyInfo->country = $request->property_country;
        $propertyInfo->city = $request->property_city;
        $propertyInfo->address = $request->property_address;
        $propertyInfo->postalcode = $request->postal_code;
        $propertyInfo->mindaysstay = 0;
        $propertyInfo->checkin = Carbon::parse('14:00:00')->format('H:i');
        $propertyInfo->checkout = Carbon::parse('00:00:00')->format('H:i');
        $propertyInfo->save();

        //Create Payment Info
        $paymentInfo = new PaymentInfo();
        $paymentInfo->requireprepay = 0;
        $paymentInfo->prepaymore7d = 0;
        $paymentInfo->prepayless7d = 0;
        $paymentInfo->save();

        //Create Invoice Info
        $invoiceInfo = new InvoiceInfo();
        $invoiceInfo->vat = 0;
        $invoiceInfo->invoiceprefix = 'INV';
        $invoiceInfo->currency = 'PHP';
        $invoiceInfo->companydetails = $request->company_details;
        $invoiceInfo->description = $request->property_desc;
        $invoiceInfo->sendinvoice = 0;
        $invoiceInfo->sendapproval = 0;
        $invoiceInfo->save();

        LogActivity::addToLog('Registered successfully: ' . $request->email . ' ' . $request->ip());

        $user->update([
            'created_ip' => $request->ip(),
            'invoice_info_id' => $invoiceInfo->id,
            'payment_info_id' => $paymentInfo->id,
            'property_info_id' => $propertyInfo->id,
        ]);
    }
}
