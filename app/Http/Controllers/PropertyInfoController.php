<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\Http\Requests\PropertyInfoRequest;
use App\PropertyInfo;

class PropertyInfoController extends Controller
{
    public function editPropertyInfo($id)
    {
        $propertyInfo = PropertyInfo::findOrFail($id);
        return view('admin.settings-propertyinfo.edit', compact('propertyInfo'));
    }

    public function updatePropertyInfo(PropertyInfoRequest $request, $id)
    {
        $propertyInfo = PropertyInfo::findOrFail($id);
        $propertyInfo->title = $request->title;
        $propertyInfo->phone = $request->phone;
        $propertyInfo->requirecc = $request->requirecc;
        $propertyInfo->type = $request->type;
        $propertyInfo->email = $request->email;
        $propertyInfo->logourl = $request->logourl;
        $propertyInfo->country = $request->country;
        $propertyInfo->city = $request->city;
        $propertyInfo->address = $request->address;
        $propertyInfo->postalcode = $request->postalcode;
        $propertyInfo->tandc = $request->tandc;
        $propertyInfo->mindaysstay = $request->mindaysstay;
        $propertyInfo->otherinfo = $request->otherinfo;
        $propertyInfo->checkin = $request->checkin;
        $propertyInfo->checkout = $request->checkout;
        $propertyInfo->coordinates = $request->coordinates;
        $propertyInfo->save();

        LogActivity::addToLog('Property Info updated successfully.');

        return redirect()->route('editPropertyInfo', auth()->user()->property_info_id)->with('success', 'Property Info updated successfully.');
    }
}
