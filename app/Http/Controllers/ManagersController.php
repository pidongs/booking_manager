<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\Http\Requests\UserRequest;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ManagersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings-managers.index');
    }

    public function getAllManagers(){
        $data = User::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function (User $user) {
                return view('admin.shared._action', [
                    'model' => $user,
                    'buttonId' => 'deleteManager',
                    'route1' => 'managers.edit',
                    'route2' => 'managers.destroy'
                ]);
            })
            ->editColumn('email', function (User $user) {
                return view('admin.shared._link', [
                    'data' => $user->id,
                    'text' => $user->email,
                    'route' => 'managers.edit'
                ]);
            })
            ->editColumn('owner_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'owner_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'owner'
                ]);
            })
            ->editColumn('booking_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'booking_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'booking'
                ]);
            })
            ->editColumn('admin_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'admin_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'admin'
                ]);
            })
            ->editColumn('calendar_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'calendar_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'calendar'
                ]);
            })
            ->editColumn('cc_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'cc_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'cc'
                ]);
            })
            ->editColumn('channelmanager_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'channelmanager_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'cm'
                ]);
            })
            ->editColumn('settings_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'settings_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'settings'
                ]);
            })
            ->editColumn('delete_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'delete_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'delete'
                ]);
            })
            ->editColumn('print_rights', function (User $user) {
                return view('admin.shared._toggle', [
                    'model' => $user,
                    'field' => 'print_rights',
                    'route' => 'manager.updateRights',
                    'name' => 'print'
                ]);
            })
            ->rawColumns(['action', 'email', 'owner_rights', 'booking_rights', 'admin_rights', 'calendar_rights', 'cc_rights', 'channelmanager_rights', 'settings_rights', 'delete_rights', 'print_rights'])
            ->make(true);
    }

    public function getManagers(Request $request){
        $columns = ['name', 'email', 'owner_rights', 'booking_rights', 'admin_rights', 'calendar_rights', 'cc_rights', 'channelmanager_rights', 'settings_rights', 'print_rights', 'created_at', 'created_ip', 'last_login', 'last_ip', 'action'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        $query = User::orderBy($columns[$column], $dir);

        if($searchValue){
            $query->where(function ($query) use ($searchValue){
                $query->where('name', 'like', '%'.$searchValue.'%')
                    ->orWhere('email', 'like', '%'.$searchValue.'%');
            });
        }

        $managers = $query->paginate($length);

        return ['data' => $managers, 'draw' => $request->draw];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createManager()
    {
        $user = new User();
        return view('admin.settings-managers.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        User::create([
            'title' => $request->title,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'created_ip' => $request->ip()
        ]);

        LogActivity::addToLog('Manager added successfully: ' . $request->title);

        return redirect()->back()->with('success', 'Manager added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function editManagerInfo($id)
    {
        $user = User::findOrFail($id);
        return view('admin.settings-managers.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateManagerInfo(Request $request, $id)
    {
        $messages = [
            'title.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'email.email' => 'Invalid email. Email must have "@" symbol and email domain ".com"',
            'email.unique' => 'Email already exists.',
            'password.min' => 'Password must be at least 8 characters.',
        ];

        if (empty($request->password)) {
            $this->validate($request, [
                'title' => 'required|max:100',
                'email' => 'required|email|max:255|unique:users,email,' . $id,
            ], $messages);

            $user = User::findOrFail($id);
            $user->title = $request->title;
            $user->email = $request->email;
            $user->save();
        } else {
            $this->validate($request, [
                'title' => 'required|max:100',
                'email' => 'required|email|max:255|unique:users,email,' . $id,
                'password' => 'min:8',
            ], $messages);

            $user = User::findOrFail($id);
            $user->title = $request->title;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
        }

        LogActivity::addToLog('Manager updated successfully: ' . $request->title);

        return redirect()->back()->with('success', 'Manager updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        LogActivity::addToLog('Manager deleted successfully: ' . $user->email);

        $user->delete();

        return redirect()->route('managers.index')->with('success', 'Manager deleted successfully.');
    }
}
