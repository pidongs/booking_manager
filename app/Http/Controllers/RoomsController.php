<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Helpers\LogActivity;
use App\Http\Requests\RoomRequest;
use App\Room;
use Datatables;
use Illuminate\Http\Request;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Room::where('user_id', auth()->user()->id)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function (Room $room) {
                    return view('admin.shared._action', [
                        'model' => $room,
                        'buttonId' => 'deleteRoom',
                        'route1' => 'rooms.edit',
                        'route2' => 'rooms.destroy',
                    ]);
                })
                ->editColumn('bookable', function (Room $room) {
                    return view('admin.shared._toggle', [
                        'model' => $room,
                        'field' => 'bookable',
                        'route' => 'rooms.updateBookable',
                        'name' => 'book',
                    ]);
                })
                ->rawColumns(['action', 'bookable'])
                ->make(true);
        }
        return view('admin.settings-rooms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRoom()
    {
        $room = new Room();
        return view('admin.settings-rooms.create', compact('room'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomRequest $request)
    {
        Room::create($request->all());

        LogActivity::addToLog('Room added successfully: ' . $request->name);

        return redirect()->back()->with('success', 'Room added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function editRoomInfo($id)
    {
        $room = Room::findOrFail($id);
        return view('admin.settings-rooms.edit', compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(RoomRequest $request, Room $room)
    {
        $room->update($request->all());

        LogActivity::addToLog('Room updated successfully: ' . $request->name);

        return redirect()->back()->with('success', 'Room updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $completedBookings = Booking::where([
                                ['room', '=', $room->id],
                                ['status', '!=', 'Completed']
                            ])->get();

        if($completedBookings->isEmpty()){
            $room->delete();
            LogActivity::addToLog('Room deleted successfully: ' . $room->name);
            return redirect()->route('rooms.index')->with('success', 'Room deleted successfully.');
        }
        else{
            LogActivity::addToLog('Unable to delete since room has an active booking: ' . $room->name);
            return redirect()->route('rooms.index')->with('error', 'Unable to delete since room has an active booking.');
        }
    }

    public function sampleDt(){
        return view('admin.settings-rooms.sample-dt');
    }

    public function dt(Request $request){

        //dd($request->all);
        $columns = ['id', 'name', 'type', 'description'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        $query = Room::orderBy($columns[$column], $dir);

        //dd($columns[$column]);

        if($searchValue){
            $query->where(function ($query) use ($searchValue){
                $query->where('id', 'like', '%'.$searchValue.'%')
                    ->orWhere('name', 'like', '%'.$searchValue.'%')
                    ->orWhere('type', 'like', '%'.$searchValue.'%')
                    ->orWhere('description', 'like', '%'.$searchValue.'%');
            });
        }

        $rooms = $query->paginate($length);
        return ['data' => $rooms, 'draw' => $request->draw];
    }
}
