<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\Http\Requests\InvoiceInfoRequest;
use App\InvoiceInfo;

class InvoiceInfoController extends Controller
{
    public function editInvoiceInfo($id)
    {
        $invoiceInfo = InvoiceInfo::findOrFail($id);
        return view('admin.settings-invoiceinfo.edit', compact('invoiceInfo'));
    }

    public function updateInvoiceInfo(InvoiceInfoRequest $request, $id)
    {
        $invoiceInfo = InvoiceInfo::findOrFail($id);
        $invoiceInfo->logourl = $request->logourl;
        $invoiceInfo->vat = $request->vat;
        $invoiceInfo->invoiceprefix = $request->invoiceprefix;
        $invoiceInfo->currency = $request->currency;
        $invoiceInfo->companydetails = $request->companydetails;
        $invoiceInfo->description = $request->description;
        $invoiceInfo->sendinvoice = $request->sendinvoice;
        $invoiceInfo->invoicetext = $request->invoicetext;
        $invoiceInfo->sendapproval = $request->sendapproval;
        $invoiceInfo->approvaltext = $request->approvaltext;
        $invoiceInfo->invoicefooter = $request->invoicefooter;
        $invoiceInfo->signaturetext = $request->signaturetext;
        $invoiceInfo->save();

        LogActivity::addToLog('Invoice Info updated successfully.');

        return redirect()->route('editInvoiceInfo', auth()->user()->invoice_info_id)->with('success', 'Invoice Info updated successfully.');
    }
}
