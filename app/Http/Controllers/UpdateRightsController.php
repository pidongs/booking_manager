<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\User;
use Illuminate\Http\Request;

class UpdateRightsController extends Controller
{
    public function updateRightsManager(Request $request)
    {
        dd($request->all());

        // $column = $request->column;
        // $id = $request->id;
        // $column_val = $request->value;

        // $user = User::findOrFail($id);
        // $user->$column = $column_val;
        // $user->save();

        // //dd($user);

        // LogActivity::addToLog('Rights updated successfully: ' . ' (' . $column . '=' . $column_val . ')');

        // return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    }

    public function updateRights(Request $request, $id, $field)
    {
        $column = $field;
        $column_val = $request->$field;
        $log_detail = $request->email;

        $this->_update($id, $column, $column_val, $log_detail);

        return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    }

    // public function updateBookingRights(Request $request, $id)
    // {
    //     $column = 'booking_rights';
    //     $column_val = $request->booking_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateDatepickerRights(Request $request, $id)
    // {
    //     $column = 'datepicker_rights';
    //     $column_val = $request->datepicker_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateAdminRights(Request $request, $id)
    // {
    //     $column = 'admin_rights';
    //     $column_val = $request->admin_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateCalendarRights(Request $request, $id)
    // {
    //     $column = 'calendar_rights';
    //     $column_val = $request->calendar_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateCcRights(Request $request, $id)
    // {
    //     $column = 'cc_rights';
    //     $column_val = $request->cc_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateChannelManagerRights(Request $request, $id)
    // {
    //     $column = 'channelmanager_rights';
    //     $column_val = $request->channelmanager_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateSettingsRights(Request $request, $id)
    // {
    //     $column = 'settings_rights';
    //     $column_val = $request->settings_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updateDeleteRights(Request $request, $id)
    // {
    //     $column = 'delete_rights';
    //     $column_val = $request->delete_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    // public function updatePrintRights(Request $request, $id)
    // {
    //     $column = 'print_rights';
    //     $column_val = $request->print_rights;
    //     $log_detail = $request->email;

    //     $this->_update($id, $column, $column_val, $log_detail);

    //     return redirect()->route('managers.index')->with('success', 'Rights updated successfully.');
    // }

    private function _update($id, $column, $column_val, $log_detail)
    {
        $user = User::findOrFail($id);
        $user->$column = $column_val;
        $user->save();

        LogActivity::addToLog('Rights updated successfully: ' . $log_detail . ' (' . $column . '=' . $column_val . ')');
    }
}
