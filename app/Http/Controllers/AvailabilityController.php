<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use App\Rate;
use App\Room;
use Carbon\Carbon;
use Datatables;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AvailabilityController extends XMLController
{
    public function availabilityCalendar()
    {
        return view('admin.availability-calendar.availability');
    }

    public function availabilityGetDates(Request $request)
    {
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date ("Y-m-d", strtotime($request->end_date));

        return redirect()->route('availabilityCalendarDate', ['from' => $start_date, 'to' => $end_date]);
    }

    public function availabilityCalendarDate($from, $to){
        $start_date = date('Y-m-d', strtotime($from));
        $end_date = date ("Y-m-d", strtotime($to));

        return view('admin.availability-calendar.availability_dates', compact('from', 'to'));
    }

    public function data(Request $request){
        $columns = [
            ['name' => 'rooms', 'label' => 'Rooms']
        ];

        $dates = [];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $s = $request->s;
        $e = $request->e;

        $start = date('Y-m-d', strtotime($s));
        $end = date ("Y-m-d", strtotime($e));
        $currentDate = strtotime($start);
        while($currentDate <= strtotime($end)){
            $formatted = date("Y-m-d", $currentDate);
            $currentDate = strtotime("+1 day", $currentDate);
            $col = [
                'name' => $formatted, 'label' => $formatted
            ];
            array_push($columns, $col);
            array_push($dates, $col);
        }

        $query = Room::latest();

        if($searchValue){
            $query->where(function ($query) use ($searchValue){
                $query->where('name', 'like', '%'.$searchValue.'%');
            });
        }

        $rooms = $query->paginate($length);

        $getRooms = Room::latest()->get();

        $fromExpedia = [];

        foreach($getRooms as $r){
            //foreach($dates as $date){
                //if($r->propertyId == null || $r->roomId == null){
                //    $temp = ['quantity' => 0, 'price' => 0];
                 //   array_push($fromExpedia, $temp);
                //}
                //else{
                   $temp = $this->_getRatesXML($r->propertyId, $r->roomId, $start, $end, $r->rateId);
                   array_push($fromExpedia, $temp);
                //}
            //}
        }

        return ['data' => $rooms, 'columns' => $columns, 'fromExpedia' => $fromExpedia, 'dates' => $dates, 'draw' => $request->draw];
    }

    public function dataAvailability(Request $request){
        $dates = [];
        $start = date('Y-m-d', strtotime($request->start_date));
        $end = date ("Y-m-d", strtotime($request->end_date));
        $currentDate = strtotime($start);
        while($currentDate <= strtotime($end)){
            $formatted = date("Y-m-d", $currentDate);
            $f = date("d-m-Y", $currentDate);
            $currentDate = strtotime("+1 day", $currentDate);
            array_push($dates, $formatted);
        }

        //$data = DB::table('rooms')->join('rates', 'rooms.id', '=', 'rates.room_id')->get();
        $data = Room::latest()->get();
        $dt = Datatables::of($data)
            ->addColumn('rooms', function (Room $room) {
                return $room->name;
            });

        foreach($dates as $date){
            $dt->addColumn($date, function($room) use ($date){
                //return $room->id.'|'.$this->_getRatesXML($room->propertyId, $room->roomId, $date);
            });
        }

        return $dt->make(true);
    }

    private function _getRatesXML($propertyId, $roomId, $start, $end, $rateId){
        $url = "https://services.expediapartnercentral.com/eqc/parr";
        $xmlRequest = "<ProductAvailRateRetrievalRQ xmlns='http://www.expediaconnect.com/EQC/PAR/2013/07'>
            <Authentication username='EQCtest12933870' password='ew67nk33'/>
            <Hotel id='".$propertyId."'/>
            <ParamSet>
                <AvailRateRetrieval from='".$start."' to='".$end."'>
                    <RoomType id='".$roomId."'>
                        <RatePlan id='".$rateId."'/>
                    </RoomType>
                </AvailRateRetrieval>
            </ParamSet>
        </ProductAvailRateRetrievalRQ>";

        $client = new Client();

        $options = [
            'headers' => [
                'Content-Type' => 'aplication/xml; charset=UTF8',
            ],
            'body' => $xmlRequest,
        ];

        $response = $client->request('POST', $url, $options);

        $array = $this->loadXmlStringAsArray($response->getBody());

        //$quantity = $array['AvailRateList']['AvailRate']['RoomType']['Inventory']['@attributes']['totalInventoryAvailable'];

        //$price = $array['AvailRateList']['AvailRate']['RoomType']['RatePlan'][0]['Rate']['PerDay']['@attributes']['rate'];
        $rrr = [];

        foreach($array['AvailRateList']['AvailRate'] as $r){
            //array_push($rrr, $r);
            $p = array_key_exists('RatePlan', $r['RoomType']) ? $r['RoomType']['RatePlan']['Rate']['PerDay']['@attributes']['rate']: 0;
            array_push($rrr, [ 'quantity' => $r['RoomType']['Inventory']['@attributes']['totalInventoryAvailable'], 'price' => $p ]);
        }

        //return $quantity.'|'.$price;
        //return ['quantity' => $quantity, 'price' => $price];
        return $rrr;
    }

    private function _updateQuantity($propertyId, $roomId, $quantity, $start, $end, $days){
        $mon = in_array('Monday', $days);
        $tue = in_array('Tuesday', $days);
        $wed = in_array('Wednesday', $days);
        $thu = in_array('Thursday', $days);
        $fri = in_array('Friday', $days);
        $sat = in_array('Saturday', $days);
        $sun = in_array('Sunday', $days);

        $s = date('Y-m-d', strtotime($start));
        $e = date ("Y-m-d", strtotime($end));

        $url = "https://services.expediapartnercentral.com/eqc/ar";
        $quantityXmlRequest = "<AvailRateUpdateRQ xmlns='http://www.expediaconnect.com/EQC/AR/2011/06'>
            <Authentication username='EQCtest12933870' password='ew67nk33' />
            <Hotel id='".$propertyId."'/>
            <AvailRateUpdate>
                <DateRange from='".$s."' to='".$e."' sun='".$sun."' mon='".$mon."' tue='".$tue."' wed='".$wed."' thu='".$thu."' fri='".$fri."' sat='".$sat."' />
                <RoomType id='".$roomId."' closed='false'>
                    <Inventory totalInventoryAvailable='".$quantity."'/>
                </RoomType>
            </AvailRateUpdate>
        </AvailRateUpdateRQ>";

        $client = new Client();

        $options = [
            'headers' => [
                'Content-Type' => 'aplication/xml; charset=UTF8',
            ],
            'body' => $quantityXmlRequest,
        ];

        $response = $client->request('POST', $url, $options);

        $array = $this->loadXmlStringAsArray($response->getBody());

        //dd($array['Success'] == []);
        //return $array['Success'] == [];
    }

    private function _updatePrice($propertyId, $roomId, $rateId, $price, $start, $end, $days){
        $mon = in_array('Monday', $days);
        $tue = in_array('Tuesday', $days);
        $wed = in_array('Wednesday', $days);
        $thu = in_array('Thursday', $days);
        $fri = in_array('Friday', $days);
        $sat = in_array('Saturday', $days);
        $sun = in_array('Sunday', $days);

        $s = date('Y-m-d', strtotime($start));
        $e = date ("Y-m-d", strtotime($end));

        $url = "https://services.expediapartnercentral.com/eqc/ar";
        $priceXmlRequest = "<AvailRateUpdateRQ xmlns='http://www.expediaconnect.com/EQC/AR/2011/06'>
            <Authentication username='EQCtest12933870' password='ew67nk33' />
            <Hotel id='".$propertyId."'/>
            <AvailRateUpdate>
            <DateRange from='".$s."' to='".$e."' sun='".$sun."' mon='".$mon."' tue='".$tue."' wed='".$wed."' thu='".$thu."' fri='".$fri."' sat='".$sat."' />
                <RoomType id='".$roomId."' closed='false'>
                    <RatePlan id='".$rateId."' closed='false'>
                        <Rate currency='USD'>
                            <PerDay rate='".$price."'/>
                        </Rate>
                    </RatePlan>
                </RoomType>
            </AvailRateUpdate>
        </AvailRateUpdateRQ>";

        $client = new Client();

        $options = [
            'headers' => [
                'Content-Type' => 'aplication/xml; charset=UTF8',
            ],
            'body' => $priceXmlRequest,
        ];

        $response = $client->request('POST', $url, $options);

        $array = $this->loadXmlStringAsArray($response->getBody());
    }

    public function getTableColumns(Request $request)
    {
        $start = date('Y-m-d', strtotime($request->start_date));
        $end = date ("Y-m-d", strtotime($request->end_date));

        $columns = [];
        array_push($columns,
            ['title' => 'Rooms', 'data' => 'name']
        );

        $currentDate = strtotime($start);
        while($currentDate <= strtotime($end)){
            $formatted = date("Y-m-d", $currentDate);
            $currentDate = strtotime("+1 day", $currentDate);
            array_push($columns, [
                'title' => $formatted,
                'data' => $formatted,
                'sortable' => false,
                'searchable' => false,
            ]);
        }
        return $columns;
    }

    public function setNewAvailabilityRates()
    {
        $rooms = Room::latest()->get();
        return view('admin.availability-calendar.set-new-availability-rates', compact('rooms'));
    }

    public function setAvailabilityRates(Request $request)
    {
        $rooms = Room::latest()->get();
        $roomId = $request->room;
        $date = $request->date;
        $option = $request->option;
        $value = $request->value;
        return view('admin.availability-calendar.set-availability-rates', compact('rooms', 'roomId', 'date', 'option', 'value'));
    }

    public function updateAvailabilityRates(Request $request){
        $messages = [
            'value.required' => 'This field is required.'
        ];

        $this->validate($request, [
            'value' => 'required|numeric'
        ], $messages);

        if(!$request->filled('room_cb') || !$request->filled('day_cb')){
            return redirect()->back()->with('error', 'Please select at least one Day and Room.');
        }
        else{
            foreach($request->room_cb as $room){
                //$ids = [];

                // $findRoom = Room::where('id', $room)->first();

                // $rateIds = Rate::where('room_id', $room)->select('id')->get();

                // foreach($rateIds as $id){
                //     array_push($ids, $id->id);
                // }

                $selectedRoom = Room::findOrFail($room);

                if($request->option == 'setQuantity'){
                    $this->_updateQuantity($selectedRoom->propertyId, $selectedRoom->roomId, $request->value, $request->start_date, $request->end_date, $request->day_cb);
                }
                else{
                    $this->_updatePrice($selectedRoom->propertyId, $selectedRoom->roomId, $selectedRoom->rateId, $request->value, $request->start_date, $request->end_date, $request->day_cb);
                }

                //$existingRoom = Rate::whereRaw("room_id='".$room."' AND start_date='".$request->start_date."' AND end_date='".$request->end_date."' ")->get();

                //If there's an existing room, update its Rate
                // if(!$existingRoom->isEmpty()){
                //     foreach($existingRoom as $r){
                //         if($request->option=='setQuantity'){
                //             $r->days = implode(",",$request->day_cb);
                //             $r->start_date = $request->start_date;
                //             $r->end_date = $request->end_date;
                //             $r->quantity = $request->value;
                //             $r->save();

                //             // $findRoom->rate_id = implode(",",$ids);
                //             // $findRoom->save();
                //         }
                //         else{
                //             $r->days = implode(",",$request->day_cb);
                //             $r->start_date = $request->start_date;
                //             $r->end_date = $request->end_date;
                //             $r->price = $request->value;
                //             $r->save();

                //             // $findRoom->rate_id = implode(",",$ids);
                //             // $findRoom->save();
                //         }
                //     }
                // }

                // //Else, create new Rate
                // else{
                //     $rate = Rate::create([
                //         'room_id' => $room,
                //         'days' => implode(",",$request->day_cb),
                //         'start_date' => $request->start_date,
                //         'end_date' => $request->end_date,
                //         'quantity' => ($request->option=='setQuantity') ? $request->value : 0,
                //         'price' => ($request->option=='setPrice') ? $request->value : ''
                //     ]);

                //     // array_push($ids, $rate->id);

                //     // $findRoom->rate_id = implode(",",$ids);
                //     // $findRoom->save();
                // }

                // LogActivity::addToLog('Rates updated successfully. Room ID: ' . $room);
            }

            return redirect()->back()->with('success', 'Rates updated successfully.');
        }
    }
}
