<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Room;
use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $today = Carbon::parse(now())->format('m/d/Y');
        $tomorrow = Carbon::parse(now()->addDay())->format('m/d/Y');

        $todayCheckin = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkin='" . $today . "' ")->count();
        $tomorrowCheckin = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkin='" . $tomorrow . "' ")->count();

        $todayCheckout = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkout='" . $today . "' ")->count();
        $tomorrowCheckout = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkout='" . $tomorrow . "' ")->count();

        return view('admin.dashboard.dashboard', compact('todayCheckin', 'tomorrowCheckin', 'todayCheckout', 'tomorrowCheckout'));
    }

    public function todayAvailable()
    {
        $route = 'todayAvailable';
        $src = 'availableToday';
        $title = 'Today\'s Available';
        return view('admin.dashboard.available', compact('route', 'src', 'title'));
    }

    public function tomorrowAvailable()
    {
        $route = 'tomorrowAvailable';
        $src = 'availableTomorrow';
        $title = 'Tomorrow\'s Available';
        return view('admin.dashboard.available', compact('route', 'src', 'title'));
    }

    public function availableToday()
    {
        $d = Carbon::parse(now())->format('m/d/Y');
        $dataBooking = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND '" . $d . "' BETWEEN checkin AND checkout")->get();

        $ids = [];

        foreach ($dataBooking as $booking) {
            array_push($ids, $booking->room);
        }

        $data = Room::where([
                        ['user_id', '=', auth()->user()->id],
                        ['bookable', '=', 1]
                      ])
                    ->whereNotIn('id', $ids)
                    ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function (Room $room) {
                return view('admin.shared._action-add', [
                    'data' => $room->id,
                    'text' => $room->name,
                    'route' => 'bookings.new',
                ]);
            })
            ->editColumn('price', function (Room $room) {
                return number_format($room->price, 2);
            })
            ->editColumn('name', function (Room $room) {
                return view('admin.shared._link', [
                    'data' => $room->id,
                    'text' => $room->name,
                    'route' => 'bookings.new',
                ]);
            })
            ->rawColumns(['action', 'name', 'price', 'quantity'])
            ->make(true);
    }

    public function availableTomorrow()
    {
        $d = Carbon::parse(now()->addDay())->format('m/d/Y');
        $dataBooking = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND '" . $d . "' BETWEEN checkin AND checkout")->get();

        $ids = [];

        foreach ($dataBooking as $booking) {
            array_push($ids, $booking->room);
        }

        $data = Room::where([
                        ['user_id', '=', auth()->user()->id],
                        ['bookable', '=', 1]
                    ])
                    ->whereNotIn('id', $ids)
                    ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function (Room $room) {
                return view('admin.shared._action-add', [
                    'data' => $room->id,
                    'text' => $room->name,
                    'route' => 'bookings.new',
                ]);
            })
            ->editColumn('price', function (Room $room) {
                return number_format($room->price, 2);
            })
            ->editColumn('name', function (Room $room) {
                return view('admin.shared._link', [
                    'data' => $room->id,
                    'text' => $room->name,
                    'route' => 'bookings.new',
                ]);
            })
            ->rawColumns(['action', 'name', 'price', 'quantity'])
            ->make(true);
    }

    public function dataBookings(Request $request){
        $columns = ['customer_name', 'balance', 'action'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $s = $request->s;
        $e = $request->e;

        $start = date('m/d/Y', strtotime($s));
        $end = date ("m/d/Y", strtotime($e));

        $query = Booking::where('user_id', auth()->user()->id)->orderBy($columns[$column], $dir);

        $query->where(function ($query) use ($start, $end){
            $query->where('checkin', '<=', $end)->where('checkout', '>=', $start);
        });

        $bookings = $query->paginate($length);

        return ['data' => $bookings, 'draw' => $request->draw];
    }
}
