<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentInfoRequest;
use App\PaymentInfo;

class PaymentInfoController extends Controller
{
    public function editPaymentInfo($id)
    {
        $paymentInfo = PaymentInfo::findOrFail($id);
        return view('admin.settings-paymentinfo.edit', compact('paymentInfo'));
    }

    public function updatePaymentInfo(PaymentInfoRequest $request, $id)
    {
        $paymentInfo = PaymentInfo::findOrFail($id);
        $paymentInfo->requireprepay = $request->requireprepay;
        $paymentInfo->prepaymore7d = $request->prepaymore7d;
        $paymentInfo->prepayless7d = $request->prepayless7d;
        $paymentInfo->prepay = $request->prepaytext;
        $paymentInfo->prepay_success = $request->prepay_success;
        $paymentInfo->prepay_fail = $request->prepay_fail;
        $paymentInfo->save();

        return redirect()->route('editPaymentInfo', auth()->user()->payment_info_id)->with('success', 'Payment Info updated successfully.');
    }
}
