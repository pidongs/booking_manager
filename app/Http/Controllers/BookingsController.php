<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Helpers\LogActivity;
use App\Http\Requests\BookingRequest;
use App\Notifications\NewBooking;
use App\Room;
use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    public function todayCheckin()
    {
        $route = 'bookings.todayCheckin';
        $today = Carbon::parse(now())->format('m/d/Y');
        $title = 'Today\'s Checkin';
        if (request()->ajax()) {
            if ($_GET["status"] == 'All') {
                $status = "";
            } else {
                $status = "AND status='" . $_GET['status'] . "'";
            }
            $data = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkin='" . $today . "' $status")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function (Booking $booking) {
                    return view('admin.shared._action-edit', [
                        'model' => $booking,
                        'route' => 'bookings.viewBooking',
                        'route1' => 'bookings.edit',
                    ]);
                })
                ->addColumn('balance', function (Booking $booking) {
                    $balance = number_format((float) $booking->total - (float) $booking->paid, 2);
                    return '<p class="text-danger">' . $balance . '</p>';
                })
                ->rawColumns(['action', 'balance'])
                ->make(true);
        }
        return view('admin.settings-bookings.index', compact('route', 'title'));
    }

    public function todayCheckout()
    {
        $today = Carbon::parse(now())->format('m/d/Y');
        $route = 'bookings.todayCheckout';
        $title = 'Today\'s Checkout';
        if (request()->ajax()) {
            if ($_GET["status"] == 'All') {
                $status = "";
            } else {
                $status = "AND status='" . $_GET['status'] . "'";
            }
            $data = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkout='" . $today . "' $status")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function (Booking $booking) {
                    return view('admin.shared._action-edit', [
                        'model' => $booking,
                        'route' => 'bookings.viewBooking',
                        'route1' => 'bookings.edit',
                    ]);
                })
                ->addColumn('balance', function (Booking $booking) {
                    $balance = number_format((float) $booking->total - (float) $booking->paid, 2);
                    return '<p class="text-danger">' . $balance . '</p>';
                })
                ->rawColumns(['action', 'balance'])
                ->make(true);
        }
        return view('admin.settings-bookings.index', compact('route', 'title'));
    }

    public function tomorrowCheckin()
    {
        $tomorrow = Carbon::parse(now()->addDay())->format('m/d/Y');
        $route = 'bookings.tomorrowCheckin';
        $title = 'Tomorrow\'s Checkin';
        if (request()->ajax()) {
            if ($_GET["status"] == 'All') {
                $status = "";
            } else {
                $status = "AND status='" . $_GET['status'] . "'";
            }
            $data = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkin='" . $tomorrow . "' $status")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function (Booking $booking) {
                    return view('admin.shared._action-edit', [
                        'model' => $booking,
                        'route' => 'bookings.viewBooking',
                        'route1' => 'bookings.edit',
                    ]);
                })
                ->addColumn('balance', function (Booking $booking) {
                    $balance = number_format((float) $booking->total - (float) $booking->paid, 2);
                    return '<p class="text-danger">' . $balance . '</p>';
                })
                ->rawColumns(['action', 'balance'])
                ->make(true);
        }
        return view('admin.settings-bookings.index', compact('route', 'title'));
    }

    public function tomorrowCheckout()
    {
        $tomorrow = Carbon::parse(now()->addDay())->format('m/d/Y');
        $route = 'bookings.tomorrowCheckout';
        $title = 'Tomorrow\'s Checkout';
        if (request()->ajax()) {
            if ($_GET["status"] == 'All') {
                $status = "";
            } else {
                $status = "AND status='" . $_GET['status'] . "'";
            }
            $data = Booking::whereRaw("user_id='" . auth()->user()->id . "' AND checkout='" . $tomorrow . "' $status")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function (Booking $booking) {
                    return view('admin.shared._action-edit', [
                        'model' => $booking,
                        'route' => 'bookings.viewBooking',
                        'route1' => 'bookings.edit',
                    ]);
                })
                ->addColumn('balance', function (Booking $booking) {
                    $balance = number_format((float) $booking->total - (float) $booking->paid, 2);
                    return '<p class="text-danger">' . $balance . '</p>';
                })
                ->rawColumns(['action', 'balance'])
                ->make(true);
        }
        return view('admin.settings-bookings.index', compact('route', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newBooking()
    {
        $rooms = Room::where([
                        ['user_id', '=', auth()->user()->id],
                        ['bookable', '=', 1]
                    ])
                    ->get();
        $r = $rooms->isEmpty();
        $booking = Booking::where('user_id', '=', auth()->user()->id)->first();
        return view('admin.settings-bookings.create', compact('rooms', 'r', 'booking'));
    }

    public function createBooking($data)
    {
        $rooms = Room::where([
                        ['user_id', '=', auth()->user()->id],
                        ['bookable', '=', 1]
                    ])
                    ->get();
        $r = $rooms->isEmpty();
        $booking = Booking::where('user_id', '=', auth()->user()->id)->first();
        return view('admin.settings-bookings.create', compact('data', 'rooms', 'r', 'booking'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingRequest $request)
    {
        Booking::create($request->all());

        LogActivity::addToLog('Booking added successfully: ' . $request->customer_name);

        return redirect()->back()->with('success', 'Booking added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function editBooking($id)
    {
        $rooms = Room::where('user_id', auth()->user()->id)->get();
        $booking = Booking::findOrFail($id);
        return view('admin.settings-bookings.edit', compact('booking', 'rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(BookingRequest $request, Booking $booking)
    {
        $booking->update($request->all());

        LogActivity::addToLog('Booking updated successfully: ' . $request->customer_name);

        return redirect()->back()->with('success', 'Booking updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }

    public function getPriceTotal(Request $request)
    {
        $room = Room::findOrFail($request->room_id);
        $room_price = (float) $room->price * (float) $request->nights;
        $guest_price = (float) $room->price_per_guest * (float) $request->guests;
        $total = (float) $room_price + (float) $guest_price;
        return response()->json([
            'price' => $room_price,
            'total' => $total,
        ]);
    }

    public function calendar()
    {
        return view('admin.settings-bookings.calendar');
    }

    public function getBookings()
    {
        $bookings = Booking::where('user_id', auth()->user()->id)->get();
        $data = [];
        foreach ($bookings as $booking) {
            $room = Room::findOrFail($booking->room);
            $event = [
                'id' => $booking->id,
                'url' => route('bookings.viewBooking', $booking->id),
                'title' => $booking->customer_name,
                'description' => 'Room: ' . $room->name . ' | Total Amount: ' . number_format($booking->total, 2) . ' | Paid: ' . number_format($booking->paid, 2),
                'start' => Carbon::parse($booking->checkin)->toDateString() . ' 00:00:00',
                'end' => Carbon::parse($booking->checkout)->toDateString() . ' 23:59:59',
            ];
            array_push($data, $event);
        }
        return response()->json($data);
    }

    public function viewBooking($id)
    {
        $booking = Booking::findOrFail($id);

        $room = Room::findOrFail($booking->room);
        $roomName = $room->name;

        $balance = number_format((float) $booking->total - (float) $booking->paid, 2);

        return view('admin.settings-bookings.view', compact('booking', 'roomName', 'balance'));
    }

    public function changeStatus(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);

        $booking->status = $request->changeStatus;
        $booking->save();

        return redirect()->back()->with('success', 'Booking updated successfully.');
    }
}
