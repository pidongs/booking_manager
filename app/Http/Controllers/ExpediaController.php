<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ExpediaController extends XMLController
{
    public function expedia(){
        return view('admin.channels.expedia.expedia');
    }

    public function getRooms(Request $request){
        $columns = ['bookable', 'name', 'map', 'check', 'active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;

        $query = Room::where('user_id', auth()->user()->id)->orderBy($columns[$column], $dir);

        $rooms = $query->paginate($length);

        return ['data' => $rooms, 'draw' => $request->draw];
    }

    public function map($id){
        $room = Room::where('id', $id)->first();
        return view('admin.channels.expedia.map', compact('room', 'id'));
    }

    public function mapRoom($id){
        $url = "https://services.expediapartnercentral.com/eqc/parr";
        $soapData = "<ProductAvailRateRetrievalRQ xmlns='http://www.expediaconnect.com/EQC/PAR/2013/07'>
            <Authentication username='EQCtest12933870' password='ew67nk33'/>
            <Hotel id='".$id."'/>
            <ParamSet>
                <ProductRetrieval/>
            </ParamSet>
        </ProductAvailRateRetrievalRQ>";

        $client = new Client();

        $options = [
            'headers' => [
                'Content-Type' => 'aplication/xml; charset=UTF8',
            ],
            'body' => $soapData,
        ];

        $response = $client->request('POST', $url, $options);

        $array = $this->loadXmlStringAsArray($response->getBody());

        $roomsFromExpedia = [];
        $rates = [];

        foreach($array['ProductList']['RoomType'] as $r){
            $tempRates = [];

            foreach($r['RatePlan'] as $rr){
                $ratesPerRoom = [
                    'id' => $rr['@attributes']['id'],
                    'name' => $rr['@attributes']['id'] . ': ' . $rr['@attributes']['name']
                ];
                array_push($tempRates, $ratesPerRoom);
            }

            $roomName = [
                'name' => $r['@attributes']['id'] . ': ' . $r['@attributes']['name'],
                'id' => $r['@attributes']['id'],
                'rates' => $tempRates
            ];

            array_push($roomsFromExpedia, $roomName);
        }

        return $roomsFromExpedia;
    }

    public function updateRoom($id, $propertyId, $roomId, $rateId){
        try{
            $room = Room::findOrFail($id);
            $room->propertyId = $propertyId;
            $room->roomId = $roomId;
            $room->rateId = $rateId;
            $room->save();
            return 'success';
        }
        catch(\Exception $e){
            return 'failed';
        }
    }
}
