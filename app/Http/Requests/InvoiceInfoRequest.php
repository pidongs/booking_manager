<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vat' => 'required|numeric',
            'currency' => 'required',
            'sendinvoice' => 'required|boolean',
            'sendapproval' => 'required|boolean',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'vat.required' => 'This field is required.',
            'currency.required' => 'This field is required.',
            'sendinvoice.required' => 'This field is required.',
            'sendapproval.required' => 'This field is required.',
            'sendinvoice.boolean' => 'Send Invoice must be 0 (true) or 1 (false).',
            'sendapproval.boolean' => 'Send Approval must be 0 (true) or 1 (false).',
        ];
    }
}
