<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'ordernumber' => 'required|numeric',
            'description' => 'required|min:20',
            'guests' => 'required|numeric|min:1|max:99',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'This field is required.',
            'ordernumber.required' => 'This field is required.',
            'guests.required' => 'This field is required.',
            'description.required' => 'This field is required.',
            'description.min' => 'Enter at least 20 characters.',
            'guests.min' => 'Guests should not be less than 1.',
            'guests.max' => 'Guests should not be greater than 99.',
        ];
    }
}
