<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'password.required' => 'This field is required.',
            'email.email' => 'Invalid email. Email must have "@" symbol and email domain ".com"',
            'password.min' => 'Password must be at least 8 characters.',
        ];
    }
}
