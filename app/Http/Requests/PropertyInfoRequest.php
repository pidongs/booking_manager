<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'phone' => 'required|numeric|digits_between:11,13',
            'type' => 'required',
            'email' => 'required|email|unique:property_infos,email,' . $this->id,
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'postalcode' => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'This field is required.',
            'phone.required' => 'This field is required.',
            'type.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'country.required' => 'This field is required.',
            'city.required' => 'This field is required.',
            'address.required' => 'This field is required.',
            'postalcode.required' => 'This field is required.',
            'email.email' => 'Invalid email. Email must have "@" symbol and email domain ".com"',
        ];
    }
}
