<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required',
            'customer_email' => 'required|email',
            'guests' => 'numeric|min:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_name.required' => 'This field is required.',
            'customer_email.required' => 'This field is required.',
            'customer_email.email' => 'Invalid email. Email must have "@" symbol and email domain ".com"',
            'guests.min' => 'Guest should be at least 1.',
            'guests.numeric' => 'Guest should be at least 1.',
        ];
    }
}
