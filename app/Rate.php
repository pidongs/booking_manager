<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'room_id',
        'days',
        'start_date',
        'end_date',
        'quantity',
        'price'
    ];
}
