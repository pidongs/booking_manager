<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Logs extends Model
{
    use Notifiable;

    protected $fillable = [
        'user', 'description', 'ip', 'user_agent', 'user_id',
    ];
}
