<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    protected $fillable = [
        'requireprepay',
        'prepaymore7d',
        'prepayless7d',
        'prepay',
        'prepay_success',
        'prepay_fail',
    ];
}
