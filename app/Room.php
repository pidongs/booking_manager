<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'type', 'ordernumber', 'description', 'safebox', 'bookable', 'guests', 'quantity', 'price', 'price_per_guest', 'photourl', 'pageurl', 'rate_id', 'user_id', 'propertyId', 'roomId', 'rateId'];

}
