<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'checkin',
        'checkout',
        'room',
        'nights',
        'guests',
        'customer_name',
        'customer_address',
        'customer_country',
        'customer_email',
        'customer_phone',
        'comment',
        'price',
        'paid',
        'total',
        'staff',
        'invoice_number',
        'status',
        'user_id'
    ];
}
