<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyInfo extends Model
{
    protected $fillable = [
        'title',
        'phone',
        'requirecc',
        'type',
        'email',
        'logourl',
        'country',
        'city',
        'address',
        'postalcode',
        'tandc',
        'mindaysstay',
        'otherinfo',
        'checkin',
        'checkout',
        'coordinates'
    ];
}
