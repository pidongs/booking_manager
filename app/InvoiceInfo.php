<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceInfo extends Model
{
    protected $fillable = [
        'logourl', 
        'vat', 
        'invoiceprefix', 
        'currency', 
        'companydetails', 
        'description', 
        'sendinvoice', 
        'invoicetext', 
        'sendapproval', 
        'approvaltext', 
        'invoicefooter', 
        'signaturetext'
    ];
}
